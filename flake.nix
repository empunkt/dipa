{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-23.05";
    };
    devenv.url = "github:cachix/devenv";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs-ruby.url = "github:bobvanderlinden/nixpkgs-ruby";
    nixpkgs-ruby.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    { self
    , nixpkgs
    , devenv
    , flake-utils
    , nixpkgs-ruby
    , ...
    }@inputs:
    flake-utils.lib.eachSystem [
      "aarch64-darwin"
      "aarch64-linux"
      "i686-linux"
      "x86_64-darwin"
      "x86_64-linux"
    ]
      (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            nixpkgs-ruby.overlays.default
          ];
        };

        rubygems = pkgs."rubygems-3.4.14";

        rubyShell = { rubyVersion }:
          let
            ruby = pkgs."ruby-${rubyVersion}".override { rubygems =  rubygems; };
          in
            devenv.lib.mkShell {
              inherit inputs pkgs;

              modules = [
                {
                  packages = [
                    pkgs.libyaml
                    pkgs.nixpkgs-fmt
                    pkgs.libmysqlclient
                    rubygems
                  ];

                  name = "Dipa dev shell with ruby version ${rubyVersion}";

                  languages = {
                    ruby = {
                      enable = true;
                      package = ruby;
                      bundler.enable = false;
                    };
                  };
                  services = {
                    mysql = {
                      enable = true;
                    };
                  };

                  env = {
                    NIXPKGS_ALLOW_INSECURE = "1";
                  };
                }
              ];
            };
      in
      {
        devShells = {
          default = rubyShell { rubyVersion = "3.2.2"; };
          ruby3_2 = rubyShell { rubyVersion = "3.2.2"; };
          ruby3_1 = rubyShell { rubyVersion = "3.1.4"; };
          ruby3_0 = rubyShell { rubyVersion = "3.0.6"; };
        };
      });
}
