# frozen_string_literal: true

FactoryBot.define do
  factory :dipa_coordinator, class: 'Dipa::Coordinator' do
    size { 1 }
    agent_queue { :ze_agent_queue }
    coordinator_queue { :ze_coordinator_queue }
    processor_class_name { 'ProcessorClassName' }
    processor_method_name { 'ProcessorMethodName' }
  end
end
