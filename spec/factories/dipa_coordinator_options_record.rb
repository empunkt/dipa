# frozen_string_literal: true

FactoryBot.define do
  factory :dipa_coordinator_options_record, class: 'Dipa::CoordinatorOptionsRecord' do
    agent_processing_timeout { 0 }
    agent_timeout { 0 }
    coordinator_processing_timeout { 0 }
    coordinator_timeout { 0 }

    coordinator factory: :dipa_coordinator
  end
end
