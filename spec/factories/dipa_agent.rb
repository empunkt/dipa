# frozen_string_literal: true

FactoryBot.define do
  factory :dipa_agent, class: 'Dipa::Agent' do
    sequence(:index, 0)

    coordinator factory: :dipa_coordinator
  end

  trait :processing do
    after(:create) do |agent|
      agent.update!(state: Dipa::Agent::COMBINED_STATES[:processing])
    end
  end

  trait :processed do
    after(:create) do |agent|
      agent.update!(state: Dipa::Agent::COMBINED_STATES[:finished])
    end
  end
end
