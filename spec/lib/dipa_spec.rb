# frozen_string_literal: true

RSpec.describe Dipa do
  describe '.map' do
    let(:source) { [1, 2, 3] }

    before do
      allow(Dipa::Processor::Map).to receive(:new).and_call_original
    end

    context 'without options' do
      it 'initializes processor' do
        described_class.map(source)

        expect(Dipa::Processor::Map).to have_received(:new)
          .with(source, options: {})
      end
    end

    context 'with options' do
      let(:options) do
        option = Dipa::Processor::Concerns::Options::OPTIONS_MAPPING.each.to_a.sample

        { option[0] => option[1][:default] }
      end

      it 'initializes processor' do
        described_class.map(source, options: options)

        expect(Dipa::Processor::Map).to have_received(:new)
          .with(source, options: options)
      end
    end
  end

  describe '.each' do
    let(:source) { [1, 2, 3] }

    before do
      allow(Dipa::Processor::Each).to receive(:new).and_call_original
    end

    context 'without options' do
      it 'initializes processor' do
        described_class.each(source)

        expect(Dipa::Processor::Each).to have_received(:new)
          .with(source, options: {})
      end
    end

    context 'with options' do
      let(:options) do
        option = Dipa::Processor::Concerns::Options::OPTIONS_MAPPING.each.to_a.sample

        { option[0] => option[1][:default] }
      end

      it 'initializes processor' do
        described_class.each(source, options: options)

        expect(Dipa::Processor::Each).to have_received(:new)
          .with(source, options: options)
      end
    end
  end
end
