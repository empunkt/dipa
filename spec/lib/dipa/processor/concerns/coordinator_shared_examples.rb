# frozen_string_literal: true

RSpec.shared_examples 'it has coordinator handling methods' do
  it_behaves_like 'it has options handling methods'

  describe '#_prepare_coordinator' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:processor_class) { 'TestProcessorClass' }
    let(:processor_method) { 'test_processor_method' }

    let(:execute) do
      test_instance.send(:_prepare_coordinator,
                         processor_class: processor_class,
                         processor_method: processor_method)
    end

    before do
      allow(test_instance).to receive(:_validate_processor_arguments)
      allow(test_instance).to receive(:_create_coordinator)
    end

    it 'calls #_validate_processor_arguments' do
      execute

      expect(test_instance).to have_received(:_validate_processor_arguments)
        .with(processor_class: processor_class,
              processor_method: processor_method)
    end

    it 'calls #_create_coordinator' do
      execute

      expect(test_instance).to have_received(:_create_coordinator)
        .with(processor_class_name: processor_class,
              processor_method_name: processor_method)
    end
  end

  describe '#_validate_processor_arguments' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) { described_class.new(source) }
    let(:processor_class) { 'Foo' }
    let(:processor_method) { 'bar' }

    context 'when processor class does not exist' do
      it 'raises Dipa::UnknownProcessorClassError' do
        expect do
          test_instance.send(:_validate_processor_arguments,
                             processor_class: processor_class,
                             processor_method: processor_method)
        end.to raise_error(Dipa::UnknownProcessorClassError)
      end
    end

    context 'when processor class exists' do
      before do
        test_class
      end

      after do
        Object.send(:remove_const, processor_class) if defined?(processor_class)
      end

      context 'when processor method does not exist' do
        let(:test_class) do
          Object.const_set(
            processor_class,
            Class.new
          )
        end

        it 'raises Dipa::Dipa::UnknownProcessorMethodError' do
          expect do
            test_instance.send(:_validate_processor_arguments,
                               processor_class: processor_class,
                               processor_method: processor_method)
          end.to raise_error(Dipa::UnknownProcessorMethodError)
        end
      end

      context 'when processor method exists' do
        let(:test_class) do
          Object.const_set(
            processor_class,
            Class.new do
              def self.bar; end
            end
          )
        end

        it 'does not raise an error' do
          expect do
            test_instance.send(:_validate_processor_arguments,
                               processor_class: processor_class,
                               processor_method: processor_method)
          end.not_to raise_error
        end
      end
    end
  end

  describe '#_create_coordinator' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:processor_class) { 'TestProcessorClass' }
    let(:processor_method) { 'test_processor_method' }
    let(:coordinator) { instance_double(Dipa::Coordinator) }
    let(:coordinator_create_params) { { coordinator: { create: :params } } }
    let(:create_params) do
      coordinator_create_params.merge(
        {
          processor_class_name: processor_class,
          processor_method_name: processor_method
        }
      )
    end

    let(:execute) do
      test_instance.send(:_create_coordinator,
                         processor_class_name: processor_class,
                         processor_method_name: processor_method)
    end

    before do
      allow(Dipa::Coordinator).to receive(:create!).and_return(coordinator)
      allow(test_instance).to receive(:_coordinator_create_params).and_return(coordinator_create_params)
      allow(test_instance).to receive(:_coordinator).and_call_original
      allow(coordinator).to receive(:dump_to_file).and_return(anything)
    end

    it 'calls #_coordinator_create_params' do
      execute

      expect(test_instance).to have_received(:_coordinator_create_params)
    end

    it 'calls #_coordinator' do
      execute

      expect(test_instance).to have_received(:_coordinator)
    end

    it 'calls Dipa::Coordinator.create!' do
      execute

      expect(Dipa::Coordinator).to have_received(:create!)
        .with(create_params)
    end

    it 'sets @_coordinator' do
      execute

      expect(test_instance.instance_variable_get(:@_coordinator)).to eq(coordinator)
    end

    it 'calls #dump_to_file on coordinator instance' do
      execute

      expect(coordinator).to have_received(:dump_to_file).with(data: source.to_a, attacher: :source_dump)
    end
  end

  describe '#_coordinator_create_params' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:agent_queue) { instance_double(Symbol) }
    let(:coordinator_queue) { instance_double(Symbol) }
    let(:coordinator_options_record_create_params) { instance_double(Hash) }
    let(:expected) do
      {
        agent_queue: agent_queue,
        coordinator_queue: coordinator_queue,
        size: source.size,
        coordinator_options_record_attributes: coordinator_options_record_create_params
      }
    end

    before do
      allow(test_instance).to receive_messages(
        _agent_queue: agent_queue,
        _coordinator_queue: coordinator_queue,
        _coordinator_options_record_create_params: coordinator_options_record_create_params
      )
    end

    it 'calls #_agent_queue' do
      test_instance.__send__(:_coordinator_create_params)

      expect(test_instance).to have_received(:_agent_queue)
    end

    it 'calls #_coordinator_queue' do
      test_instance.__send__(:_coordinator_create_params)

      expect(test_instance).to have_received(:_coordinator_queue)
    end

    it 'calls #_coordinator_options_record_create_params' do
      test_instance.__send__(:_coordinator_create_params)

      expect(test_instance).to have_received(:_coordinator_options_record_create_params)
    end

    it 'returns expected result' do
      expect(test_instance.__send__(:_coordinator_create_params)).to eq(expected)
    end
  end

  describe '#_coordinator_options_record_create_params' do
    boolean_methods = %i[
      _keep_data?
      _want_result?
    ]
    integer_methods = %i[
      _agent_timeout
      _agent_processing_timeout
      _coordinator_timeout
      _coordinator_processing_timeout
    ]

    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:expected) do
      boolean_methods.concat(integer_methods).each_with_object({}) do |i, a|
        a[i.to_s.delete_prefix('_').delete_suffix('?').to_sym] = public_send(i)
      end
    end

    boolean_methods.each { |m| let(m) { instance_double([TrueClass, FalseClass].sample) } }
    integer_methods.each { |m| let(m) { instance_double(Integer) } }

    before do
      boolean_methods.concat(integer_methods).each do |method|
        allow(test_instance).to receive(method).and_return(public_send(method))
      end
    end

    boolean_methods.concat(integer_methods).each do |method|
      it "calls #{method}" do
        test_instance.__send__(:_coordinator_options_record_create_params)

        expect(test_instance).to have_received(method)
      end
    end

    it 'returns expected params' do
      expect(test_instance.__send__(:_coordinator_options_record_create_params)).to eq(expected)
    end
  end

  describe '#_coordinator_waiting_state?' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_coordinator) { build(:dipa_coordinator) }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
      allow(_coordinator).to receive(:reload)
      allow(_coordinator).to receive(:finished_state?).and_return(finished_state?)
    end

    context 'when finished' do
      let(:finished_state?) { true }

      it 'calls #_coordinator twice' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(test_instance).to have_received(:_coordinator).twice
      end

      it 'calls #reload on coordinator instance' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(_coordinator).to have_received(:reload)
      end

      it 'calls #finished_state? on coordinator instance' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(_coordinator).to have_received(:finished_state?)
      end

      it 'returns false' do
        expect(test_instance.send(:_coordinator_waiting_state?)).to be_falsey
      end
    end

    context 'when not finished' do
      let(:finished_state?) { false }

      it 'calls #_coordinator twice' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(test_instance).to have_received(:_coordinator).twice
      end

      it 'calls #reload on coordinator instance' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(_coordinator).to have_received(:reload)
      end

      it 'calls #finished_state? on coordinator instance' do
        test_instance.send(:_coordinator_waiting_state?)

        expect(_coordinator).to have_received(:finished_state?)
      end

      it 'returns true' do
        expect(test_instance.send(:_coordinator_waiting_state?)).to be_truthy
      end
    end
  end

  describe '#_result' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_coordinator) { instance_double(Dipa::Coordinator) }

    before do
      allow(_coordinator).to receive(:want_result?).and_return(want_result?)
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
      allow(_coordinator).to receive(:result)
      allow(_coordinator).to receive(:reload)
      allow(_coordinator).to receive(:source)
    end

    context 'when want_result?' do
      let(:want_result?) { true }
      let(:result) { [4, 5, 6] }

      before do
        allow(_coordinator).to receive(:result).and_return(result)
      end

      it 'calls #reload on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:reload)
      end

      it 'calls #want_result? on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:want_result?)
      end

      it 'calls #_coordinator three times' do
        test_instance.send(:_result)

        expect(test_instance).to have_received(:_coordinator).exactly(3).times
      end

      it 'calls #result on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:result)
      end

      it 'does not call #source on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).not_to have_received(:source)
      end

      it 'returns "result"' do
        expect(test_instance.send(:_result)).to be(result)
      end
    end

    context 'when not want_result?' do
      let(:want_result?) { false }
      let(:source) { [7, 8, 9] }

      before do
        allow(_coordinator).to receive(:source).and_return(source)
      end

      it 'calls #reload on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:reload)
      end

      it 'calls #want_result? on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:want_result?)
      end

      it 'calls #_coordinator three times' do
        test_instance.send(:_result)

        expect(test_instance).to have_received(:_coordinator).exactly(3).times
      end

      it 'calls #source on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).to have_received(:source)
      end

      it 'does not call #result on coordinator instance' do
        test_instance.send(:_result)

        expect(_coordinator).not_to have_received(:result)
      end

      it 'returns "source"' do
        expect(test_instance.send(:_result)).to be(source)
      end
    end
  end
end
