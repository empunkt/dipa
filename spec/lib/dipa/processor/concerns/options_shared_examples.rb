# frozen_string_literal: true

RSpec.shared_examples 'it has options handling methods' do
  describe '#_validate_options' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:options) { {} }

    before do
      allow(options).to receive(:assert_valid_keys)
    end

    it 'calls #assert_valid_keys on options hash' do
      test_instance.send(:_validate_options, options: options)

      expect(options).to have_received(:assert_valid_keys)
        .with(*Dipa::Processor::Concerns::Options::OPTIONS_MAPPING.keys)
    end
  end

  describe '#_write_options' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:options) { { foo: :bar, baz: :quux } }

    before do
      # mock away the call from #initialize
      allow(test_instance).to receive(:_write_options).with(options: {})
      allow(test_instance).to receive(:_write_options).and_call_original
      allow(test_instance).to receive(:_write_provided_options)
      allow(test_instance).to receive(:_write_unprovided_options)
    end

    it 'calls #_write_provided_options' do
      test_instance.send(:_write_options, options: options)

      expect(test_instance).to have_received(:_write_provided_options)
        .with(options: options)
    end

    it 'calls #_write_unprovided_options' do
      test_instance.send(:_write_options, options: options)

      expect(test_instance).to have_received(:_write_unprovided_options)
        .with(options: options)
    end
  end

  describe '#_write_provided_options' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:options) { { foo: :bar, baz: :quux } }

    before do
      # mock away the call from #initialize
      allow(test_instance).to receive(:_write_options).with(options: {})
      allow(test_instance).to receive(:_write_option)
    end

    it "calls #_write_option 'option.length' times" do
      test_instance.send(:_write_provided_options, options: options)

      expect(test_instance).to have_received(:_write_option)
        .exactly(options.length).times
    end

    it 'calls #_write_option with first kew/value pair' do
      test_instance.send(:_write_provided_options, options: options)

      expect(test_instance).to have_received(:_write_option)
        .with(*options.to_a[0])
    end

    it 'calls #_write_option with second kew/value pair' do
      test_instance.send(:_write_provided_options, options: options)

      expect(test_instance).to have_received(:_write_option)
        .with(*options.to_a[1])
    end
  end

  describe '#_write_unprovided_options' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:test_options) do
      {
        agent_queue: :ze_agent_queue,
        coordinator_queue: :ze_coordinator_queue,
        timeout: 23,
        agent_timeout: 42,
        coordinator_timeout: 23 + 42,
        async: true,
        keep_data: true,
        want_result: false
      }
    end

    before do
      # mock away the call from #initialize
      allow(test_instance).to receive(:_write_options).with(options: {})
      allow(test_instance).to receive(:_write_option)
    end

    Dipa::Processor::Concerns::Options::OPTIONS_MAPPING.each_key do |option|
      it 'does not call #_write_option for given option' do
        test_instance.send(:_write_unprovided_options,
                           options: { option => test_options[option] })

        expect(test_instance).not_to have_received(:_write_option)
          .with(option, test_options[option])
      end

      context "with given option :#{option}" do
        unprovided_options = Dipa::Processor::Concerns::Options::OPTIONS_MAPPING
                             .except(option)

        unprovided_options.each do |default_option, value|
          it "calls #_write_option for default option :#{default_option}" do
            test_instance.send(:_write_unprovided_options,
                               options: { option => test_options[option] })

            expect(test_instance).to have_received(:_write_option)
              .with(default_option, value[:default])
          end
        end
      end
    end
  end

  describe '#_write_option' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end

    options = {
      agent_queue: :ze_agent_queue,
      coordinator_queue: :ze_coordinator_queue,
      agent_timeout: 23,
      agent_processing_timeout: 42,
      coordinator_timeout: 23 + 42,
      coordinator_processing_timeout: 23 + 42 + 23
    }

    options.each do |option, value|
      context "with '#{option}' option" do
        let(:_value_with_override_check) do
          case value
          when Symbol
            "new_#{value}".to_sym
          when Integer
            value + 1
          when FalseClass, TrueClass
            !value
          end
        end

        let(:config_writer) do
          "#{Dipa::Processor::Concerns::Options::
             OPTIONS_MAPPING[option][:accessor]}=".to_sym
        end

        before do
          allow(test_instance).to receive(:_value_with_override_check)
            .and_return(_value_with_override_check)
          allow(test_instance).to receive(config_writer)
        end

        it 'calls #_value_with_override_check' do
          test_instance.send(:_write_option, option, value)

          expect(test_instance).to have_received(:_value_with_override_check)
            .with(option, value)
        end

        it 'calls config writer' do
          test_instance.send(:_write_option, option, value)

          expect(test_instance).to have_received(config_writer)
            .with(_value_with_override_check)
        end
      end
    end
  end

  describe '#_value_with_override_check' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:option) { :ze_option }
    let(:value) { :ze_value }
    let(:override_value) { :ze_override_value }

    context 'with override option' do
      it 'returns override value' do
        stub_const("#{described_class}::OVERRIDE_OPTIONS",
                   { option => override_value })

        expect(
          test_instance.send(:_value_with_override_check, option, value)
        ).to eq(override_value)
      end
    end

    context 'without override option' do
      it 'returns override value' do
        stub_const("#{described_class}::OVERRIDE_OPTIONS", {})

        expect(
          test_instance.send(:_value_with_override_check, option, value)
        ).to eq(value)
      end
    end
  end
end
