# frozen_string_literal: true

RSpec.shared_examples 'it has state handling methods' do
  describe '#_processing_state?' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end

    before do
      test_instance.instance_variable_set(
        :@_processing_state, _processing_state
      )
    end

    context 'when @_processing_state is nil' do
      let(:_processing_state) { nil }

      it 'returns false' do
        expect(test_instance.send(:_processing_state?)).to be(false)
      end
    end

    context 'when @_processing_state is false' do
      let(:_processing_state) { false }

      it 'returns false' do
        expect(test_instance.send(:_processing_state?)).to be(false)
      end
    end

    context 'when @_processing_state is true' do
      let(:_processing_state) { true }

      it 'returns true' do
        expect(test_instance.send(:_processing_state?)).to be(true)
      end
    end
  end

  describe '#_check_state' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end

    before do
      allow(test_instance).to receive(:_processing_state?)
        .and_return(_processing_state)
    end

    context 'when #_processing_state is true' do
      let(:_processing_state) { true }

      it 'calls #_processing_state?' do
        test_instance.send(:_check_state)
      rescue StandardError
        expect(test_instance).to have_received(:_processing_state?)
      end

      it 'raises Dipa::AlreadyProcessingError' do
        expect do
          test_instance.send(:_check_state)
        end.to raise_error(Dipa::AlreadyProcessingError)
      end

      it 'does not change @_processing_state' do
        test_instance.instance_variable_set(:@_processing_state, true)

        test_instance.send(:_check_state)
      rescue StandardError
        expect(test_instance.instance_variable_get(:@_processing_state))
          .to be(true)
      end
    end

    context 'when #_processing_state is false' do
      let(:_processing_state) { false }

      it 'calls #_processing_state?' do
        test_instance.send(:_check_state)
      rescue StandardError
        expect(test_instance).to have_received(:_processing_state?)
      end

      it 'does not raise an error' do
        expect { test_instance.send(:_check_state) }.not_to raise_error
      end

      it 'changes #_processing_state? response' do
        expect { test_instance.send(:_check_state) }.to(
          change { test_instance.instance_variable_get(:@_processing_state) }
            .from(nil).to(true)
        )
      end
    end
  end

  describe '#_reset_state' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end

    before do
      test_instance.instance_variable_set(:@_processing_state, true)
    end

    it 'sets @_processing_state to false' do
      expect { test_instance.send(:_reset_state) }.to(
        change { test_instance.instance_variable_get(:@_processing_state) }
          .from(true).to(false)
      )
    end
  end
end
