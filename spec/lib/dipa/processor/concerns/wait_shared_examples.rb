# frozen_string_literal: true

RSpec.shared_examples 'it has wait handling methods' do
  it_behaves_like 'it has coordinator handling methods'
  it_behaves_like 'it has options handling methods'

  describe '#_wait_for_it_with_timeout' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) { described_class.new(source) }
    let(:_wait_for_it) { [4, 5, 6] }

    before do
      allow(test_instance).to receive(:_init_timeout_and_sleep_variables)
      allow(test_instance).to receive(:_wait_for_it).and_return(_wait_for_it)
    end

    it 'calls #_init_timeout_and_sleep_variables' do
      test_instance.send(:_wait_for_it_with_timeout)

      expect(test_instance).to have_received(:_init_timeout_and_sleep_variables)
    end

    it 'calls #_wait_for_it' do
      test_instance.send(:_wait_for_it_with_timeout)

      expect(test_instance).to have_received(:_wait_for_it)
    end

    it 'returns expected result' do
      expect(test_instance.send(:_wait_for_it_with_timeout)).to eq(_wait_for_it)
    end
  end

  describe '#_init_timeout_and_sleep_variables' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) { described_class.new(source) }
    let(:_start) { 1.hour.ago }

    around do |example|
      Timecop.freeze(_start) do
        example.run
      end
    end

    it 'initializes @_start' do
      expect do
        test_instance.send(:_init_timeout_and_sleep_variables)
      end.to change { test_instance.instance_variable_get(:@_start) }
        .from(nil).to(_start)
    end

    it 'initializes @_sleep_duration' do
      expect do
        test_instance.send(:_init_timeout_and_sleep_variables)
      end.to change { test_instance.instance_variable_get(:@_sleep_duration) }
        .from(nil).to(Dipa::Processor::Concerns::Wait::
                      SYNC_MODE_SLEEP_START_DURATION)
    end
  end

  describe '#_wait_for_it' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) { described_class.new(source) }
    let(:_coordinator_waiting_state?) { [true, false] }
    let(:_coordinator) { build(:dipa_coordinator) }

    before do
      allow(test_instance).to receive(:_coordinator_waiting_state?)
        .and_return(*_coordinator_waiting_state?)
      allow(test_instance).to receive(:_sleep)
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
    end

    context 'when finished' do
      let(:finished?) { true }
      let(:_result) { [4, 5, 6] }

      before do
        allow(_coordinator).to receive(:finished?).and_return(finished?)
        allow(test_instance).to receive(:_result).and_return(_result)
        allow(test_instance).to receive(:_raise_error)
      end

      it 'calls #_coordinator_waiting_state? twice' do
        test_instance.send(:_wait_for_it)

        expect(test_instance).to have_received(:_coordinator_waiting_state?)
          .twice
      end

      it 'calls #_sleep' do
        test_instance.send(:_wait_for_it)

        expect(test_instance).to have_received(:_sleep)
      end

      it 'calls #_coordinator' do
        test_instance.send(:_wait_for_it)

        expect(test_instance).to have_received(:_coordinator)
      end

      it 'calls #finished? on coordinator instance' do
        test_instance.send(:_wait_for_it)

        expect(_coordinator).to have_received(:finished?)
      end

      it 'calls #_result' do
        test_instance.send(:_wait_for_it)

        expect(test_instance).to have_received(:_result)
      end

      it 'does not call #_raise_error' do
        test_instance.send(:_wait_for_it)

        expect(test_instance).not_to have_received(:_raise_error)
      end

      it 'returns result' do
        expect(test_instance.send(:_wait_for_it)).to be(_result)
      end
    end

    context 'when not finished' do
      let(:finished?) { false }

      before do
        allow(_coordinator).to receive(:finished?).and_return(finished?)
        allow(test_instance).to receive(:_result)
        allow(test_instance).to receive(:_raise_error).and_raise
      end

      it 'calls #_coordinator_waiting_state? twice' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(test_instance).to have_received(:_coordinator_waiting_state?)
          .twice
      end

      it 'calls #_coordinator' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(test_instance).to have_received(:_coordinator)
      end

      it 'calls #_sleep' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(test_instance).to have_received(:_sleep)
      end

      it 'calls #finished? on coordinator instance' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(_coordinator).to have_received(:finished?)
      end

      it 'does not call #_result' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(test_instance).not_to have_received(:_result)
      end

      it 'calls #_raise_error' do
        test_instance.send(:_wait_for_it)
      rescue StandardError
        expect(test_instance).to have_received(:_raise_error)
      end
    end
  end

  describe '#_sleep' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_sleep_duration) { 23 }

    before do
      test_instance.instance_variable_set(:@_sleep_duration, _sleep_duration)
      allow(test_instance).to receive(:sleep)
      allow(test_instance).to receive(:_adjust_sleep_duration)
    end

    it 'calls #sleep' do
      test_instance.send(:_sleep)

      expect(test_instance).to have_received(:sleep).with(_sleep_duration)
    end

    it 'calls #_adjust_sleep_duration' do
      test_instance.send(:_sleep)

      expect(test_instance).to have_received(:_adjust_sleep_duration)
    end
  end

  describe '#_adjust_sleep_duration' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:max_sleep_duration) { 23 }
    let(:increase_factor) { 2 }

    before do
      stub_const(
        'Dipa::Processor::Concerns::Wait::SYNC_MODE_MAX_SLEEP_DURATION',
        max_sleep_duration
      )
      stub_const(
        'Dipa::Processor::Concerns::Wait::' \
        'SYNC_MODE_SLEEP_DURATION_INCREASE_FACTOR',
        increase_factor
      )
      test_instance.instance_variable_set(:@_sleep_duration, _sleep_duration)
    end

    context 'when @_sleep_duration < SYNC_MODE_MAX_SLEEP_DURATION' do
      let(:_sleep_duration) { 22 }

      it 'increases @_sleep_duration by defined factor' do
        test_instance.send(:_adjust_sleep_duration)

        expect(
          test_instance.instance_variable_get(:@_sleep_duration)
        ).to eq(_sleep_duration * increase_factor)
      end
    end

    context 'when @_sleep_duration = SYNC_MODE_MAX_SLEEP_DURATION' do
      let(:_sleep_duration) { 23 }

      it 'increases @_sleep_duration by defined factor' do
        test_instance.send(:_adjust_sleep_duration)

        expect(
          test_instance.instance_variable_get(:@_sleep_duration)
        ).to eq(_sleep_duration * increase_factor)
      end
    end

    context 'when @_sleep_duration > SYNC_MODE_MAX_SLEEP_DURATION' do
      let(:_sleep_duration) { 24 }

      it 'does not increases @_sleep_duration' do
        test_instance.send(:_adjust_sleep_duration)

        expect(
          test_instance.instance_variable_get(:@_sleep_duration)
        ).to eq(_sleep_duration)
      end
    end
  end

  describe '#_raise_error' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_coordinator) { build(:dipa_coordinator) }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
      allow(_coordinator).to receive(:failed_state?).and_return(failed_state?)
    end

    shared_examples 'it raises failed state error' do
      let(:failed_state?) { true }
      let(:error_class_name) { "Dipa::Coordinator#{failed_state.to_s.camelize}Error" }

      before do
        allow(_coordinator).to receive(:state).and_return(failed_state)
      end

      it 'calls #_coordinator twice' do
        test_instance.send(:_raise_error)
      rescue StandardError
        expect(test_instance).to have_received(:_coordinator).twice
      end

      it 'calls #failed_state? on coordinator instance' do
        test_instance.send(:_raise_error)
      rescue StandardError
        expect(_coordinator).to have_received(:failed_state?)
      end

      it 'calls #state on coordinator instance' do
        test_instance.send(:_raise_error)
      rescue StandardError
        expect(_coordinator).to have_received(:state)
      end

      it do
        expect do
          test_instance.send(:_raise_error)
        end.to raise_error(error_class_name)
      end
    end

    Dipa::Coordinator::COMBINED_FINISHED_STATES.except(:finished).each_key do |state|
      context "when failed state is `#{state}`" do
        let(:failed_state) { state }

        it_behaves_like 'it raises failed state error'
      end
    end

    context 'when coordinator has no error state' do
      let(:failed_state?) { false }

      it 'calls #_coordinator' do
        test_instance.send(:_raise_error)
      rescue StandardError
        expect(test_instance).to have_received(:_coordinator)
      end

      it 'calls #failed_state? on coordinator instance' do
        test_instance.send(:_raise_error)
      rescue StandardError
        expect(_coordinator).to have_received(:failed_state?)
      end

      it 'raises Dipa::UnknownProcessingStateError' do
        expect do
          test_instance.send(:_raise_error)
        end.to raise_error(Dipa::UnknownProcessingStateError)
      end
    end
  end
end
