# frozen_string_literal: true

RSpec.describe Dipa::Processor::Each do
  it 'has override options' do
    expect(described_class::OVERRIDE_OPTIONS).to eq({ want_result: false })
  end
end
