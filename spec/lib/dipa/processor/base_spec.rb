# frozen_string_literal: true

RSpec.describe Dipa::Processor::Base do
  it_behaves_like 'it has coordinator handling methods'
  it_behaves_like 'it has options handling methods'
  it_behaves_like 'it has state handling methods'
  it_behaves_like 'it has wait handling methods'

  describe '#with' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) { described_class.new(source) }
    let(:processor_class) { 'ProcessorClassName' }
    let(:processor_method) { 'processor_method_name' }

    before do
      allow(test_instance).to receive(:_check_state)
      allow(test_instance).to receive(:_prepare_coordinator)
      allow(test_instance).to receive(:_start_process)
      allow(test_instance).to receive(:_result_and_cleanup)
      allow(test_instance).to receive(:_reset_state)
    end

    it 'calls #_check_state' do
      test_instance.with(processor_class, processor_method)

      expect(test_instance).to have_received(:_check_state)
    end

    it 'calls #_prepare_coordinator' do
      test_instance.with(processor_class, processor_method)

      expect(test_instance).to have_received(:_prepare_coordinator)
        .with(processor_class: processor_class,
              processor_method: processor_method)
    end

    it 'calls #_start_process' do
      test_instance.with(processor_class, processor_method)

      expect(test_instance).to have_received(:_start_process)
    end

    it 'calls #_wait_for_it_with_timeout' do
      test_instance.with(processor_class, processor_method)

      expect(test_instance).to have_received(:_result_and_cleanup)
    end

    it 'calls #_reset_state' do
      test_instance.with(processor_class, processor_method)

      expect(test_instance).to have_received(:_reset_state)
    end

    context 'when an error raises' do
      before do
        allow(test_instance).to receive(:_start_process).and_raise
      end

      it 'calls #_reset_state' do
        test_instance.with(processor_class, processor_method)
      rescue RuntimeError
        expect(test_instance).to have_received(:_reset_state)
      end
    end
  end

  describe '#initialize' do
    let(:source) { [1, 2, 3] }
    let(:options) { { foo: :bar } }

    before do
      # rubocop:disable RSpec/AnyInstance
      allow_any_instance_of(described_class).to receive(:_validate_options)
      allow_any_instance_of(described_class).to receive(:_write_options)
      # rubocop:enable RSpec/AnyInstance
    end

    it 'sets @_source' do
      test_instance = described_class.new(source, options: options)

      expect(test_instance.instance_variable_get(:@_source)).to be(source)
    end

    it 'calls #_validate_options' do
      test_instance = described_class.new(source, options: options)

      expect(test_instance).to have_received(:_validate_options)
        .with(options: options)
    end

    it 'calls #_write_options' do
      test_instance = described_class.new(source, options: options)

      expect(test_instance).to have_received(:_write_options)
        .with(options: options)
    end
  end

  describe '#_result_and_cleanup' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end

    before do
      allow(test_instance).to receive(:_after_result_action)
    end

    context 'without error' do
      before do
        allow(test_instance).to receive(:_wait_for_it_with_timeout)
      end

      it 'does not raise' do
        expect { test_instance.send(:_result_and_cleanup) }.not_to raise_error
      end

      it 'calls #_wait_for_it_with_timeout' do
        test_instance.send(:_result_and_cleanup)

        expect(test_instance).to have_received(:_wait_for_it_with_timeout)
      end

      it 'calls #_after_result_action' do
        test_instance.send(:_result_and_cleanup)

        expect(test_instance).to have_received(:_after_result_action)
      end
    end

    context 'with error' do
      let(:error_message) { 'ze error message' }

      before do
        allow(test_instance).to receive(:_wait_for_it_with_timeout).and_raise(error_message)
      end

      it 'raises' do
        expect { test_instance.send(:_result_and_cleanup) }.to raise_error(error_message)
      end

      it 'calls #_wait_for_it_with_timeout' do
        test_instance.send(:_result_and_cleanup)
      rescue StandardError
        expect(test_instance).to have_received(:_wait_for_it_with_timeout)
      end

      it 'calls #_after_result_action' do
        test_instance.send(:_result_and_cleanup)
      rescue StandardError
        expect(test_instance).to have_received(:_after_result_action)
      end
    end
  end

  describe '#_after_result_action' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_coordinator) { build(:dipa_coordinator) }

    before do
      allow(
        Dipa::CoordinatorServices::MaybeCleanupService
      ).to receive(:call).and_return(anything)
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
    end

    it 'calls Dipa::CoordinatorServices::MaybeCleanupService.call' do
      test_instance.send(:_after_result_action)

      expect(
        Dipa::CoordinatorServices::MaybeCleanupService
      ).to have_received(:call).with(coordinator: _coordinator)
    end
  end

  describe '#_start_process' do
    let(:source) { [1, 2, 3] }
    let(:test_instance) do
      described_class.new(source)
    end
    let(:_coordinator) { build(:dipa_coordinator) }

    before do
      allow(
        Dipa::CoordinatorServices::StartProcessingService
      ).to receive(:call).and_return(anything)
      allow(test_instance).to receive(:_coordinator).and_return(_coordinator)
    end

    it 'calls Dipa::CoordinatorServices::StartProcessingService.call' do
      test_instance.send(:_start_process)

      expect(
        Dipa::CoordinatorServices::StartProcessingService
      ).to have_received(:call).with(coordinator: _coordinator)
    end
  end
end
