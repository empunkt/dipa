# frozen_string_literal: true

Rails.application.routes.draw do
  mount Dipa::Engine => '/dipa'
end
