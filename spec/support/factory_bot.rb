# frozen_string_literal: true

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.definition_file_paths = [
      File.join(
        File.expand_path('../..', __dir__), 'spec/factories'
      )
    ]
    FactoryBot.find_definitions

    DatabaseCleaner.start
    FactoryBot.lint
    DatabaseCleaner.clean
  end

  config.after do
    FactoryBot.rewind_sequences
  end
end
