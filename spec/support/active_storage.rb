# frozen_string_literal: true

RSpec.configure do |config|
  config.after do
    FileUtils.rm_rf(ActiveStorage::Blob.service.root)
  end
end
