# frozen_string_literal: true

RSpec.describe Dipa::DateValidator, type: :model do
  let(:test_class) do
    Struct.new('Foo', :date) do
      include ActiveModel::Validations

      validates :date, 'dipa/date' => true
    end
  end

  after do
    Struct.send(:remove_const, :Foo) if defined?(Struct::Foo)
  end

  [
    '2019-01-21',
    '2019-01-21 06:41:10 UTC',
    Date.parse('2019-01-21'),
    Time.now.utc
  ].each do |date|
    context "with \"#{date}\"" do
      let(:model) { test_class.new(date) }

      it 'is valid' do
        expect(model).to be_valid
      end
    end
  end

  [
    'foo',
    '',
    nil
  ].each do |date|
    context "with \"#{date}\"" do
      let(:model) { test_class.new(date) }

      it 'is invalid' do
        expect(model).not_to be_valid
      end

      it 'has expected errors' do
        model.valid?

        expect(model.errors[:date]).to match_array('is not valid Date')
      end
    end
  end
end
