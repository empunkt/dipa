# frozen_string_literal: true

RSpec.describe Dipa::Coordinator do
  it_behaves_like 'it has dumpable method', :source_dump
  it_behaves_like 'it has loadable method', :source_dump

  it_behaves_like 'it has coordinator state attributes'

  context 'with test instance for shared examples' do
    let(:test_instance) { create(:dipa_coordinator) }

    it_behaves_like 'it has state attributes'
  end

  describe 'delegators' do
    let(:coordinator_options_record) { create(:dipa_coordinator_options_record) }
    let(:test_instance) { coordinator_options_record.coordinator }

    delegate_methods = %i[
      keep_data?
      want_result?
      agent_timeout
      agent_processing_timeout
      coordinator_timeout
      coordinator_processing_timeout
    ]

    before do
      delegate_methods.each do |delegate_method|
        allow(coordinator_options_record).to receive(delegate_method)
      end
    end

    delegate_methods.each do |delegate_method|
      it "delegates ##{delegate_method} to #coordinator_options_record" do
        test_instance.__send__(delegate_method)

        expect(coordinator_options_record).to have_received(delegate_method)
      end
    end
  end

  describe '#result' do
    let(:test_instance) { create(:dipa_coordinator) }

    before do
      allow(test_instance).to receive(:finished?).and_return(finished?)
    end

    context 'when not processed' do
      let(:finished?) { false }

      before do
        allow(test_instance).to receive(:_result_from_agents)
      end

      it 'calls #finished?' do
        test_instance.result

        expect(test_instance).to have_received(:finished?)
      end

      it 'returns nil' do
        expect(test_instance.result).to be_nil
      end
    end

    context 'when processed' do
      let(:finished?) { true }
      let(:_result_from_agents) { [anything] }

      before do
        allow(test_instance).to receive(:_result_from_agents)
          .and_return(_result_from_agents)
      end

      it 'calls #finished?' do
        test_instance.result

        expect(test_instance).to have_received(:finished?)
      end

      it 'returns result' do
        expect(test_instance.result).to eq(_result_from_agents)
      end
    end
  end

  describe '#source' do
    let(:test_instance) { create(:dipa_coordinator) }
    let(:source) { [anything] }

    before do
      allow(test_instance).to receive(:load_from_file).and_return(source)
    end

    it 'calls #load_from_file' do
      test_instance.source

      expect(test_instance).to have_received(:load_from_file)
        .with(attacher: :source_dump)
    end

    it 'returns source' do
      expect(test_instance.source).to eq(source)
    end
  end

  describe 'all_agents_created_and_finished?' do
    let(:test_instance) { create(:dipa_coordinator) }

    before do
      allow(test_instance).to receive(:_all_agents_created?)
        .and_return(_all_agents_created?)
    end

    context 'without all agents created' do
      let(:_all_agents_created?) { false }

      before do
        allow(test_instance).to receive(:_all_agents_finished?)
      end

      it 'calls #_all_agents_created?' do
        test_instance.all_agents_created_and_finished?

        expect(test_instance).to have_received(:_all_agents_created?)
      end

      it 'does not call #_all_agents_finished?' do
        test_instance.all_agents_created_and_finished?

        expect(test_instance).not_to have_received(:_all_agents_finished?)
      end

      it 'returns false' do
        expect(test_instance.all_agents_created_and_finished?).to be(false)
      end
    end

    context 'with all agents created' do
      let(:_all_agents_created?) { true }

      before do
        allow(test_instance).to receive(:_all_agents_finished?)
          .and_return(_all_agents_finished?)
      end

      context 'without all agents processed' do
        let(:_all_agents_finished?) { false }

        it 'calls #_all_agents_created?' do
          test_instance.all_agents_created_and_finished?

          expect(test_instance).to have_received(:_all_agents_created?)
        end

        it 'calls #_all_agents_finished?' do
          test_instance.all_agents_created_and_finished?

          expect(test_instance).to have_received(:_all_agents_finished?)
        end

        it 'returns false' do
          expect(test_instance.all_agents_created_and_finished?).to be(false)
        end
      end

      context 'with all agents processed' do
        let(:_all_agents_finished?) { true }

        it 'calls #_all_agents_created?' do
          test_instance.all_agents_created_and_finished?

          expect(test_instance).to have_received(:_all_agents_created?)
        end

        it 'calls #_all_agents_finished?' do
          test_instance.all_agents_created_and_finished?

          expect(test_instance).to have_received(:_all_agents_finished?)
        end

        it 'returns true' do
          expect(test_instance.all_agents_created_and_finished?).to be(true)
        end
      end
    end
  end

  describe '#timeout_reached?' do
    let(:time) { Time.current }
    let(:created_at) { time - 1.hour }
    let(:test_instance) { create(:dipa_coordinator, created_at: created_at) }

    before do
      allow(test_instance).to receive(:coordinator_timeout).and_return(coordinator_timeout)
    end

    around do |example|
      Timecop.freeze(time) { example.run }
    end

    context 'when coordinator_timeout is zero' do
      let(:coordinator_timeout) { 0 }

      it 'calls #coordinator_timeout once' do
        test_instance.timeout_reached?

        expect(test_instance).to have_received(:coordinator_timeout).once
      end

      it 'returns false' do
        expect(test_instance).not_to be_timeout_reached
      end
    end

    context 'when coordinator processing timeout is not reached' do
      let(:coordinator_timeout) { 3600 }

      it 'calls #coordinator_timeout twice' do
        test_instance.timeout_reached?

        expect(test_instance).to have_received(:coordinator_timeout).twice
      end

      it 'returns false' do
        expect(test_instance).not_to be_timeout_reached
      end
    end

    context 'when coordinator processing timeout is reached' do
      let(:coordinator_timeout) { 3599 }

      it 'calls #coordinator_timeout twice' do
        test_instance.timeout_reached?

        expect(test_instance).to have_received(:coordinator_timeout).twice
      end

      it 'returns true' do
        expect(test_instance).to be_timeout_reached
      end
    end
  end

  describe '#processing_timeout_reached?' do
    let(:time) { Time.current }
    let(:started_at) { time - 1.hour }
    let(:test_instance) { create(:dipa_coordinator, started_at: started_at) }

    before do
      allow(test_instance).to receive(:coordinator_processing_timeout).and_return(coordinator_processing_timeout)
    end

    around do |example|
      Timecop.freeze(time) { example.run }
    end

    context 'when coordinator_processing_timeout is zero' do
      let(:coordinator_processing_timeout) { 0 }

      it 'calls #coordinator_processing_timeout once' do
        test_instance.processing_timeout_reached?

        expect(test_instance).to have_received(:coordinator_processing_timeout).once
      end

      it 'returns false' do
        expect(test_instance).not_to be_processing_timeout_reached
      end
    end

    context 'when coordinator processing timeout is not reached' do
      let(:coordinator_processing_timeout) { 3600 }

      it 'calls #coordinator_processing_timeout twice' do
        test_instance.processing_timeout_reached?

        expect(test_instance).to have_received(:coordinator_processing_timeout).twice
      end

      it 'returns false' do
        expect(test_instance).not_to be_processing_timeout_reached
      end
    end

    context 'when coordinator processing timeout is reached' do
      let(:coordinator_processing_timeout) { 3599 }

      it 'calls #coordinator_processing_timeout twice' do
        test_instance.processing_timeout_reached?

        expect(test_instance).to have_received(:coordinator_processing_timeout).twice
      end

      it 'returns true' do
        expect(test_instance).to be_processing_timeout_reached
      end
    end
  end

  describe '#_result_from_agents' do
    let(:test_instance) { create(:dipa_coordinator, size: 2) }

    let(:first_agent) do
      create(:dipa_agent, index: 1, coordinator: test_instance)
    end
    let(:second_agent) do
      create(:dipa_agent, index: 0, coordinator: test_instance)
    end
    let(:first_agent_result) { 'LOL!' }
    let(:second_agent_result) { 'ROFL!' }

    before do
      first_agent.result_dump
                 .attach(
                   io: StringIO.new(Marshal.dump(first_agent_result), 'rb'),
                   filename: 'foo.txt'
                 )
      first_agent.finished!
      second_agent.result_dump
                  .attach(
                    io: StringIO.new(Marshal.dump(second_agent_result), 'rb'),
                    filename: 'foo.txt'
                  )
      second_agent.finished!
    end

    it 'returns result in correct order' do
      expect(
        test_instance.send(:_result_from_agents)
      ).to eq([second_agent_result, first_agent_result])
    end
  end

  describe '#_all_agents_finished?' do
    let(:test_instance) { create(:dipa_coordinator, size: 2) }
    let(:first_agent) { create(:dipa_agent, coordinator: test_instance) }
    let(:second_agent) { create(:dipa_agent, coordinator: test_instance) }

    context 'with all agents processed' do
      before do
        allow(first_agent).to receive(:finished?).and_return(true)
        allow(second_agent).to receive(:finished?).and_return(true)
      end

      it 'calls #finished? on first agent instance' do
        test_instance.send(:_all_agents_finished?)

        expect(first_agent).to have_received(:finished?)
      end

      it 'calls #finished? on second agent instance' do
        test_instance.send(:_all_agents_finished?)

        expect(second_agent).to have_received(:finished?)
      end

      it 'returns true' do
        expect(test_instance.send(:_all_agents_finished?)).to be_truthy
      end
    end

    context 'without all agents processed' do
      before do
        allow(first_agent).to receive(:finished?).and_return(true)
        allow(second_agent).to receive(:finished?).and_return(false)
      end

      it 'calls #finished? on first agent instance' do
        test_instance.send(:_all_agents_finished?)

        expect(first_agent).to have_received(:finished?)
      end

      it 'calls #finished? on second agent instance' do
        test_instance.send(:_all_agents_finished?)

        expect(second_agent).to have_received(:finished?)
      end

      it 'returns false' do
        expect(test_instance.send(:_all_agents_finished?)).to be_falsey
      end
    end
  end

  describe '#_all_agents_created?' do
    context 'without all agents' do
      let(:test_instance) do
        create(:dipa_coordinator, size: 2) do |coordinator|
          create(:dipa_agent, coordinator: coordinator)
        end
      end

      it 'returns false' do
        expect(test_instance.send(:_all_agents_created?)).to be(false)
      end
    end

    context 'with all agents' do
      let(:test_instance) do
        create(:dipa_coordinator, size: 2) do |coordinator|
          create_list(:dipa_agent, 2, coordinator: coordinator)
        end
      end

      it 'returns true' do
        expect(test_instance.send(:_all_agents_created?)).to be(true)
      end
    end
  end
end
