# frozen_string_literal: true

RSpec.describe Dipa::Agent do
  it_behaves_like 'it has dumpable method', :source_dump
  it_behaves_like 'it has dumpable method', :result_dump
  it_behaves_like 'it has loadable method', :source_dump
  it_behaves_like 'it has loadable method', :result_dump

  it_behaves_like 'it has agent state attributes'

  context 'with test instance for shared examples' do
    let(:test_instance) { create(:dipa_agent) }

    it_behaves_like 'it has state attributes'
  end

  describe '#result' do
    let(:test_instance) { create(:dipa_agent) }

    before do
      allow(test_instance).to receive(:finished?).and_return(finished?)
    end

    context 'when finished' do
      let(:finished?) { true }
      let(:result) { anything }

      before do
        allow(test_instance).to receive(:load_from_file).and_return(result)
      end

      it 'calls #finished?' do
        test_instance.result

        expect(test_instance).to have_received(:finished?)
      end

      it 'calls #load_from_file' do
        test_instance.result

        expect(test_instance).to have_received(:load_from_file)
          .with(attacher: :result_dump)
      end

      it 'returns result' do
        expect(test_instance.result).to eq(result)
      end
    end

    context 'when not finished' do
      let(:finished?) { false }

      before do
        allow(test_instance).to receive(:load_from_file)
      end

      it 'calls #finished?' do
        test_instance.result

        expect(test_instance).to have_received(:finished?)
      end

      it 'does not call #load_from_file' do
        test_instance.result

        expect(test_instance).not_to have_received(:load_from_file)
      end
    end
  end

  describe '#source' do
    let(:test_instance) { create(:dipa_agent) }
    let(:source) { anything }

    before do
      allow(test_instance).to receive(:load_from_file).and_return(source)
    end

    it 'calls #load_from_file' do
      test_instance.source

      expect(test_instance).to have_received(:load_from_file)
        .with(attacher: :source_dump)
    end

    it 'returns source' do
      expect(test_instance.source).to eq(source)
    end
  end

  describe '#timeout_reached?' do
    let(:test_instance) { create(:dipa_agent) }
    let(:created_at) { 1.hour.ago }

    before do
      Timecop.freeze(created_at) { test_instance }

      allow(test_instance.coordinator).to receive(:agent_timeout).and_return(agent_timeout)
    end

    context 'when agent timeout is zero' do
      let(:agent_timeout) { 0 }

      it 'calls #agent_timeout on coordinator instance once' do
        test_instance.timeout_reached?

        expect(test_instance.coordinator).to have_received(:agent_timeout).once
      end

      it 'returns false' do
        expect(test_instance.timeout_reached?).to be(false)
      end
    end

    context 'when agent timeout is not zero' do
      let(:agent_timeout) { 5 }

      context 'when timeout is not reached' do
        around do |example|
          Timecop.freeze(created_at + agent_timeout) do
            example.run
          end
        end

        it 'calls #agent_timeout on coordinator instance twice' do
          test_instance.timeout_reached?

          expect(test_instance.coordinator).to have_received(:agent_timeout).twice
        end

        it 'returns false' do
          expect(test_instance.timeout_reached?).to be(false)
        end
      end

      context 'when timeout is reached' do
        around do |example|
          Timecop.freeze(created_at + agent_timeout + 1) do
            example.run
          end
        end

        it 'calls #agent_timeout on coordinator instance twice' do
          test_instance.timeout_reached?

          expect(test_instance.coordinator).to have_received(:agent_timeout).twice
        end

        it 'returns true' do
          expect(test_instance.timeout_reached?).to be(true)
        end
      end
    end
  end

  describe '#processing_timeout_reached?' do
    let(:test_instance) { create(:dipa_agent) }
    let(:created_at) { 2.hours.ago }
    let(:started_at) { created_at + 1.hour }

    before do
      Timecop.freeze(created_at) do
        test_instance.update!(started_at: started_at)
      end

      allow(test_instance.coordinator).to receive(:agent_processing_timeout).and_return(agent_processing_timeout)
    end

    context 'when agent timeout is zero' do
      let(:agent_processing_timeout) { 0 }

      it 'calls #agent_processing_timeout on coordinator instance once' do
        test_instance.processing_timeout_reached?

        expect(test_instance.coordinator).to have_received(:agent_processing_timeout).once
      end

      it 'returns false' do
        expect(test_instance.processing_timeout_reached?).to be(false)
      end
    end

    context 'when agent timeout is not zero' do
      let(:agent_processing_timeout) { 5 }

      context 'when timeout is not reached' do
        around do |example|
          Timecop.freeze(started_at + agent_processing_timeout) do
            example.run
          end
        end

        it 'calls #agent_processing_timeout on coordinator instance twice' do
          test_instance.processing_timeout_reached?

          expect(test_instance.coordinator).to have_received(:agent_processing_timeout).twice
        end

        it 'returns false' do
          expect(test_instance.processing_timeout_reached?).to be(false)
        end
      end

      context 'when timeout is reached' do
        around do |example|
          Timecop.freeze(started_at + agent_processing_timeout + 1) do
            example.run
          end
        end

        it 'calls #agent_processing_timeout on coordinator instance twice' do
          test_instance.processing_timeout_reached?

          expect(test_instance.coordinator).to have_received(:agent_processing_timeout).twice
        end

        it 'returns true' do
          expect(test_instance.processing_timeout_reached?).to be(true)
        end
      end
    end
  end
end
