# frozen_string_literal: true

RSpec.describe Dipa::ApplicationRecord do
  describe 'configuration' do
    it 'is an abstract class' do
      expect(described_class.abstract_class).to be(true)
    end

    context 'when initialized' do
      it 'raises NotImplementedError' do
        expect { described_class.new }.to raise_error(NotImplementedError)
      end
    end
  end
end
