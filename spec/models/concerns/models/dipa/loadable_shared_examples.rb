# frozen_string_literal: true

RSpec.shared_examples 'it has loadable method' do |attacher_method|
  describe '#load_from_file' do
    let(:test_instance) do
      create(described_class.name.downcase.gsub('::', '_').to_sym)
    end

    before do
      allow(test_instance).to receive(attacher_method).and_call_original
      allow(test_instance.public_send(attacher_method)).to receive(:attached?)
        .and_return(attached?)
    end

    context 'when attached' do
      let(:attached?) { true }
      let(:marshal_dump) { 'serialized_string' }
      let(:data) { anything }

      before do
        test_instance.public_send(attacher_method)
                     .attach(io: StringIO.new('FOO!'), filename: 'foo.txt')
        allow(test_instance.public_send(attacher_method)).to receive(:download)
          .and_return(marshal_dump)
        allow(Marshal).to receive(:load).and_return(data)
      end

      it "calls ##{attacher_method} on #{described_class} instance twice" do
        test_instance.load_from_file(attacher: attacher_method)

        # - will be called 3 times in the before blocks
        expect(test_instance).to have_received(attacher_method).exactly(5).times
      end

      it 'calls #attached? on attacher' do
        test_instance.load_from_file(attacher: attacher_method)

        expect(
          test_instance.public_send(attacher_method)
        ).to have_received(:attached?)
      end

      it 'calls #download on attacher' do
        test_instance.load_from_file(attacher: attacher_method)

        expect(
          test_instance.public_send(attacher_method)
        ).to have_received(:download)
      end

      it 'calls Marshal.load' do
        test_instance.load_from_file(attacher: attacher_method)

        expect(Marshal).to have_received(:load).with(marshal_dump)
      end

      it 'returns data' do
        expect(
          test_instance.load_from_file(attacher: attacher_method)
        ).to eq(data)
      end
    end

    context 'when not attached' do
      let(:attached?) { false }

      before do
        allow(Marshal).to receive(:load)
      end

      it "calls ##{attacher_method} on #{described_class} instance twice" do
        test_instance.load_from_file(attacher: attacher_method)

        expect(test_instance).to have_received(attacher_method).twice
      end

      it 'does not call Marshal.load' do
        test_instance.load_from_file(attacher: attacher_method)

        expect(Marshal).not_to have_received(:load)
      end

      it 'returns nil' do
        expect(
          test_instance.load_from_file(attacher: attacher_method)
        ).to be_nil
      end
    end
  end
end
