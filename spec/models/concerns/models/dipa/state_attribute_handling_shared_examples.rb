# frozen_string_literal: true

RSpec.shared_examples 'it has state attributes' do
  it_behaves_like 'it has methods to set constants'

  shared_examples 'it has exclamation mark method for state' do |state|
    describe "##{state}!" do
      context 'with desired state' do
        before do
          test_instance.update!(state: state)
        end

        it 'does not change state' do
          expect { test_instance.__send__("#{state}!".to_sym) }.not_to change(test_instance, :state)
        end
      end

      context 'without desired state' do
        before do
          other_state = described_class::COMBINED_STATES.values.sample
          other_state = described_class::COMBINED_STATES.values.sample until state != other_state

          test_instance.update!(state: other_state)
        end

        it 'changes state' do
          expect { test_instance.__send__("#{state}!".to_sym) }.to change(test_instance, :state).to(state)
        end
      end
    end
  end

  shared_examples 'it has question mark method for state' do |state|
    describe "##{state}?" do
      context 'with desired state' do
        before do
          test_instance.update!(state: state)
        end

        it 'returns true' do
          expect(test_instance.public_send("#{state}?".to_sym)).to be(true)
        end
      end

      context 'without desired state' do
        before do
          other_state = described_class::COMBINED_STATES.values.sample
          other_state = described_class::COMBINED_STATES.values.sample until state != other_state

          test_instance.update!(state: other_state)
        end

        it 'returns false' do
          expect(test_instance.public_send("#{state}?".to_sym)).to be(false)
        end
      end
    end
  end

  described_class::COMBINED_STATES.each_value do |state|
    it_behaves_like 'it has question mark method for state', state
  end

  describe 'constants' do
    %i[
      SHARED_FINISHED_STATES
      SHARED_CONTINUING_STATES
      COMBINED_FINISHED_STATES
      COMBINED_CONTINUING_STATES
      COMBINED_STATES
    ].each do |konst|
      it "has constant `#{konst}` defined" do
        expect(described_class).to be_const_defined(konst)
      end
    end
  end

  describe 'custom callbacks' do
    describe 'test #_set_started_at callback' do
      let(:time) { 1.hour.from_now }

      around do |example|
        Timecop.freeze(time) do
          example.run
        end
      end

      before do
        allow(test_instance).to receive(:_set_started_at).and_call_original
        allow(test_instance).to receive(:_processing_started?).and_return(_processing_started?)
      end

      context 'when processing has not started' do
        let(:_processing_started?) { false }

        it 'calls #_processing_started' do
          test_instance.validate

          expect(test_instance).to have_received(:_processing_started?)
        end

        it 'does not call #_set_started_at' do
          test_instance.validate

          expect(test_instance).not_to have_received(:_set_started_at)
        end

        it 'does not set `started_at` attribute' do
          test_instance.validate

          expect(test_instance.started_at).to be_nil
        end
      end

      context 'when processing has started' do
        let(:_processing_started?) { true }

        it 'calls #_processing_started' do
          test_instance.validate

          expect(test_instance).to have_received(:_processing_started?)
        end

        it 'calls #_set_started_at' do
          test_instance.validate

          expect(test_instance).to have_received(:_set_started_at)
        end

        it 'sets `started_at` attribute' do
          expect { test_instance.validate }.to change(test_instance, :started_at)
            .from(nil).to(time.floor(Dipa::DATETIME_PRECISION))
        end
      end
    end

    describe 'test #_set_finished_at callback' do
      let(:time) { 1.hour.from_now }

      around do |example|
        Timecop.freeze(time) do
          example.run
        end
      end

      before do
        allow(test_instance).to receive(:_set_finished_at).and_call_original
        allow(test_instance).to receive(:_processing_finished?).and_return(_processing_finished?)
      end

      context 'when processing has not started' do
        let(:_processing_finished?) { false }

        it 'calls #_processing_started' do
          test_instance.validate

          expect(test_instance).to have_received(:_processing_finished?)
        end

        it 'does not call #_set_finished_at' do
          test_instance.validate

          expect(test_instance).not_to have_received(:_set_finished_at)
        end

        it 'does not set `started_at` attribute' do
          test_instance.validate

          expect(test_instance.finished_at).to be_nil
        end
      end

      context 'when processing has started' do
        let(:_processing_finished?) { true }

        it 'calls #_processing_started' do
          test_instance.validate

          expect(test_instance).to have_received(:_processing_finished?)
        end

        it 'calls #_set_finished_at' do
          test_instance.validate

          expect(test_instance).to have_received(:_set_finished_at)
        end

        it 'sets `finished_at` attribute' do
          expect { test_instance.validate }.to change(test_instance, :finished_at)
            .from(nil).to(time.floor(Dipa::DATETIME_PRECISION))
        end
      end
    end
  end

  describe '#finished_state?' do
    before do
      allow(test_instance).to receive(:state).and_return(state)
    end

    context 'when state is finished state' do
      let(:state) { test_instance.class::COMBINED_FINISHED_STATES.values.sample }

      it 'calls #state' do
        test_instance.finished_state?

        expect(test_instance).to have_received(:state)
      end

      it 'returns truthy value' do
        expect(test_instance).to be_finished_state
      end
    end

    context 'when state is not finished state' do
      let(:state) { :started }

      it 'calls #state' do
        test_instance.finished_state?

        expect(test_instance).to have_received(:state)
      end

      it 'returns falsey value' do
        expect(test_instance).not_to be_finished_state
      end
    end
  end

  describe '#failed_state?' do
    before do
      allow(test_instance).to receive(:state).and_return(state)
    end

    context 'when state is failed state' do
      let(:state) { test_instance.class::COMBINED_FINISHED_STATES.except(:finished).values.sample }

      it 'calls #state' do
        test_instance.failed_state?

        expect(test_instance).to have_received(:state)
      end

      it 'returns truthy value' do
        expect(test_instance).to be_failed_state
      end
    end

    context 'when state is not failed state' do
      let(:state) { :finished }

      it 'calls #state' do
        test_instance.failed_state?

        expect(test_instance).to have_received(:state)
      end

      it 'returns falsey value' do
        expect(test_instance).not_to be_failed_state
      end
    end
  end

  describe '#_set_started_at' do
    let(:time) { 1.hour.from_now.floor(Dipa::DATETIME_PRECISION) }

    around do |example|
      Timecop.freeze(time) do
        example.run
      end
    end

    it 'changes `started_at` attribute to current time' do
      expect { test_instance.__send__(:_set_started_at) }.to change(test_instance, :started_at).from(nil).to(time)
    end
  end

  describe '#_set_finished_at' do
    let(:time) { 1.hour.from_now.floor(Dipa::DATETIME_PRECISION) }

    around do |example|
      Timecop.freeze(time) do
        example.run
      end
    end

    it 'changes `finished_at` attribute to current time' do
      expect { test_instance.__send__(:_set_finished_at) }.to change(test_instance, :finished_at).from(nil).to(time)
    end
  end

  describe '#_processing_started?' do
    before do
      allow(test_instance).to receive(:state_changed?).and_return(state_changed?)
    end

    context 'when state changed' do
      let(:state_changed?) { true }

      it 'calls #state_changed?' do
        test_instance.__send__(:_processing_started?)

        expect(test_instance).to have_received(:state_changed?)
          .with(from: test_instance.class::COMBINED_STATES[:initialized],
                to: test_instance.class::COMBINED_STATES[:processing])
      end

      it 'returns truthy value' do
        expect(test_instance.__send__(:_processing_started?)).to be_truthy
      end
    end

    context 'when state didn\t change' do
      let(:state_changed?) { false }

      it 'calls #state_changed?' do
        test_instance.__send__(:_processing_started?)

        expect(test_instance).to have_received(:state_changed?)
          .with(from: test_instance.class::COMBINED_STATES[:initialized],
                to: test_instance.class::COMBINED_STATES[:processing])
      end

      it 'returns falsey value' do
        expect(test_instance.__send__(:_processing_started?)).to be_falsey
      end
    end
  end

  describe '#_processing_finished?' do
    before do
      allow(test_instance).to receive_messages(state_changed?: state_changed?, state_change: [anything, to_state])
    end

    context 'when state changed' do
      let(:state_changed?) { true }

      context 'when state changed to finished state' do
        let(:to_state) { described_class::COMBINED_FINISHED_STATES.values.sample }

        it 'calls #state_changed?' do
          test_instance.__send__(:_processing_finished?)

          expect(test_instance).to have_received(:state_changed?)
            .with(from: test_instance.class::COMBINED_STATES[:processing])
        end

        it 'calls #state_change' do
          test_instance.__send__(:_processing_finished?)

          expect(test_instance).to have_received(:state_change)
        end

        it 'returns truthy value' do
          expect(test_instance.__send__(:_processing_finished?)).to be_truthy
        end
      end

      context "when state didn't change to finished state" do
        let(:to_state) { described_class::COMBINED_CONTINUING_STATES.values.sample }

        it 'calls #state_changed?' do
          test_instance.__send__(:_processing_finished?)

          expect(test_instance).to have_received(:state_changed?)
            .with(from: test_instance.class::COMBINED_STATES[:processing])
        end

        it 'calls #state_change' do
          test_instance.__send__(:_processing_finished?)

          expect(test_instance).to have_received(:state_change)
        end

        it 'returns falsey value' do
          expect(test_instance.__send__(:_processing_finished?)).to be_falsey
        end
      end
    end

    context "when state didn't change" do
      let(:state_changed?) { false }
      let(:to_state) { anything }

      it 'calls #state_changed?' do
        test_instance.__send__(:_processing_finished?)

        expect(test_instance).to have_received(:state_changed?)
          .with(from: test_instance.class::COMBINED_STATES[:processing])
      end

      it 'does not call #state_change' do
        test_instance.__send__(:_processing_finished?)

        expect(test_instance).not_to have_received(:state_change)
      end

      it 'returns falsey value' do
        expect(test_instance.__send__(:_processing_finished?)).to be_falsey
      end
    end
  end
end
