# frozen_string_literal: true

RSpec.shared_examples 'it has dumpable method' do |attacher_method|
  describe '#dump_to_file' do
    let(:test_instance) do
      create(described_class.name.downcase.gsub('::', '_').to_sym)
    end
    let(:data) { { foo: :bar } }
    let(:marshal_dump) { 'serialized_string' }
    let(:io) { StringIO.new }
    let(:attacher) do
      ActiveStorage::Attached::One.new(attacher_method, test_instance)
    end
    let(:attach) { true }

    before do
      allow(Marshal).to receive(:dump).and_return(marshal_dump)
      allow(StringIO).to receive(:new).and_return(io)
      allow(test_instance).to receive(attacher_method).and_return(attacher)
      allow(attacher).to receive(:attach).and_return(attach)
    end

    it 'calls Marshal.dump' do
      test_instance.dump_to_file(data: data, attacher: attacher_method)

      expect(Marshal).to have_received(:dump).with(data)
    end

    it 'calls StringIO.new' do
      test_instance.dump_to_file(data: data, attacher: attacher_method)

      expect(StringIO).to have_received(:new).with(marshal_dump, 'rb')
    end

    it "calls ##{attacher_method} on #{described_class} instance" do
      test_instance.dump_to_file(data: data, attacher: attacher_method)

      expect(test_instance).to have_received(attacher_method)
    end

    it 'calls #attach on attacher' do
      test_instance.dump_to_file(data: data, attacher: attacher_method)

      expect(attacher).to have_received(:attach)
        .with(io: io, filename: "#{attacher_method}.dat")
    end

    it 'returns attach result' do
      expect(
        test_instance.dump_to_file(data: data, attacher: attacher_method)
      ).to eq(attach)
    end
  end
end
