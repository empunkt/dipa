# frozen_string_literal: true

RSpec.shared_examples 'it has methods to set constants' do
  describe '.set_constant' do
    let(:test_class) { described_class }
    let(:konst) { SecureRandom.__send__(:choose, [*'A'..'Z'], 16).to_sym }

    context 'when constant is already defined' do
      let(:konst_value) { 'quux' }

      before do
        described_class.const_set(konst, konst_value)

        allow(described_class).to receive(:const_defined?).and_call_original
        allow(described_class).to receive(:const_set).and_call_original
      end

      it 'calls .const_defined?' do
        described_class.set_constant(konst: konst, value: anything)

        expect(described_class).to have_received(:const_defined?).with(konst)
      end

      it 'does not call .const_set' do
        described_class.set_constant(konst: konst, value: anything)

        expect(described_class).not_to have_received(:const_set)
      end

      it 'has expected value' do
        described_class.set_constant(konst: konst, value: anything)

        expect("#{described_class}::#{konst}".constantize).to eq(konst_value)
      end
    end

    context 'when constant is not defined' do
      let(:konst_value) { 'quux' }

      before do
        allow(described_class).to receive(:const_defined?).and_call_original
        allow(described_class).to receive(:const_set).and_call_original
      end

      it 'calls .const_defined?' do
        described_class.set_constant(konst: konst, value: konst_value)

        expect(described_class).to have_received(:const_defined?).with(konst)
      end

      it 'calls .const_set' do
        described_class.set_constant(konst: konst, value: konst_value)

        expect(described_class).to have_received(:const_set).with(konst, konst_value)
      end

      it 'has expected value' do
        described_class.set_constant(konst: konst, value: konst_value)

        expect("#{described_class}::#{konst}".constantize).to eq(konst_value)
      end
    end
  end

  describe '.set_from_merged_constants' do
    let(:test_class) { described_class }
    let(:konst) { SecureRandom.__send__(:choose, [*'A'..'Z'], 16).to_sym }
    let(:first_from) { SecureRandom.__send__(:choose, [*'A'..'Z'], 16).to_sym }
    let(:second_from) { SecureRandom.__send__(:choose, [*'A'..'Z'], 16).to_sym }
    let(:first_from_value) { { foo: :bar } }
    let(:second_from_value) { { baz: :quux } }
    let(:konst_value) { first_from_value.merge(second_from_value) }

    before do
      described_class.const_set(first_from, first_from_value)
      described_class.const_set(second_from, second_from_value)

      allow(described_class).to receive(:const_get).and_call_original
      allow(described_class).to receive(:set_constant)
    end

    it 'calls .const_get for first from constant' do
      described_class.set_from_merged_constants(konst: konst, first_from: first_from, second_from: second_from)

      expect(described_class).to have_received(:const_get).with(first_from)
    end

    it 'calls .const_get for second from constant' do
      described_class.set_from_merged_constants(konst: konst, first_from: first_from, second_from: second_from)

      expect(described_class).to have_received(:const_get).with(second_from)
    end

    it 'calls .set_constant' do
      described_class.set_from_merged_constants(konst: konst, first_from: first_from, second_from: second_from)

      expect(described_class).to have_received(:set_constant).with(konst: konst, value: konst_value)
    end
  end
end
