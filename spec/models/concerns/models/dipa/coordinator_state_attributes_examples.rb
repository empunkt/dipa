# frozen_string_literal: true

RSpec.shared_examples 'it has coordinator state attributes' do
  it_behaves_like 'it has methods to set constants'

  context 'when module is included' do
    let(:test_class) { Class.new }

    before do
      # .set_constant must be defined before stubbing
      test_class.include(Dipa::Models::SettingConstants)

      allow(test_class).to receive(:set_constant)
    end

    it 'calls .set_constant and sets extra finished states' do
      test_class.include(Dipa::Models::CoordinatorStateAttributes)

      expect(test_class).to have_received(:set_constant).with(
        konst: :EXTRA_FINISHED_STATES,
        value: Dipa::Models::CoordinatorStateAttributes::COORDINATOR_EXTRA_FINISHED_STATES
      )
    end

    it 'calls .set_constant and sets extra continuing states' do
      test_class.include(Dipa::Models::CoordinatorStateAttributes)

      expect(test_class).to have_received(:set_constant).with(
        konst: :EXTRA_CONTINUING_STATES,
        value: Dipa::Models::CoordinatorStateAttributes::COORDINATOR_EXTRA_CONTINUING_STATES
      )
    end
  end
end
