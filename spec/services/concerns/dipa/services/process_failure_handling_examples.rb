# frozen_string_literal: true

RSpec.shared_examples 'it handles process failures' do
  let(:test_instance) { described_class.new }

  describe '#_stop_processing?' do
    let(:agent) { instance_double(Dipa::Agent) }

    context 'with stop processing state' do
      before do
        Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
          allow(test_instance).to receive("_#{state}_state_desired?".to_sym).and_return(state == desired_state)
        end
      end

      shared_examples 'it checks agent for stop state' do |state|
        let(:desired_state) { state }

        state_index = Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.index(state)

        Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each_with_index do |test_state, i|
          expect_call = i <= state_index

          it "#{expect_call ? 'calls' : 'does not call'} #_#{test_state}_state_desired?" do
            test_instance.__send__(:_stop_processing?, agent: agent)

            expect(test_instance).__send__(
              expect_call ? :to : :not_to, have_received("_#{test_state}_state_desired?".to_sym).with(agent: agent)
            )
          end
        end

        it 'returns true' do
          expect(test_instance.__send__(:_stop_processing?, agent: agent)).to be_truthy
        end
      end

      Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
        it_behaves_like 'it checks agent for stop state', state
      end
    end

    context 'without stop processing state' do
      before do
        Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
          allow(test_instance).to receive("_#{state}_state_desired?".to_sym).and_return(false)
        end
      end

      Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |test_state|
        it "calls #_#{test_state}_state_desired?" do
          test_instance.__send__(:_stop_processing?, agent: agent)

          expect(test_instance).to have_received("_#{test_state}_state_desired?".to_sym).with(agent: agent)
        end
      end

      it 'returns false' do
        expect(test_instance.__send__(:_stop_processing?, agent: agent)).to be_falsey
      end
    end
  end

  describe '#_stop_processing!' do
    let(:agent) { instance_double(Dipa::Agent) }
    let(:thread) { instance_double(Thread) }
    let(:agent_state) { instance_double(Symbol) }
    let(:error_class_name) { instance_double(String) }

    before do
      allow(test_instance).to receive_messages(_determine_agent_state: agent_state,
                                               _determine_error_class_name: error_class_name)
      allow(test_instance).to receive(:_end_it_all!)
    end

    it 'calls #_determine_agent_state' do
      test_instance.__send__(:_stop_processing!, agent: agent, thread: thread)

      expect(test_instance).to have_received(:_determine_agent_state).with(agent: agent)
    end

    it 'calls #_determine_error_class_name' do
      test_instance.__send__(:_stop_processing!, agent: agent, thread: thread)

      expect(test_instance).to have_received(:_determine_error_class_name).with(state: agent_state)
    end

    it 'calls #_end_it_all!' do
      test_instance.__send__(:_stop_processing!, agent: agent, thread: thread)

      expect(test_instance).to have_received(:_end_it_all!)
        .with(agent: agent, thread: thread, state: agent_state, error_class_name: error_class_name)
    end
  end

  describe '#_determine_error_class_name' do
    let(:state) { :foo_bar_baz }
    let(:expected) { 'Dipa::AgentFooBarBazError' }

    it 'returns correct error class name' do
      expect(test_instance.__send__(:_determine_error_class_name, state: state)).to eq(expected)
    end
  end

  describe '#_determine_agent_state' do
    let(:agent) { instance_double(Dipa::Agent) }
    let(:desired_state) { instance_double(Symbol) }

    before do
      allow(test_instance).to receive_messages(
        _determine_agent_current_state: current_state,
        _determine_agent_desired_state: desired_state
      )
    end

    context 'with current state' do
      let(:current_state) { instance_double(Symbol) }

      it 'calls #_determine_agent_current_state' do
        test_instance.__send__(:_determine_agent_state, agent: agent)

        expect(test_instance).to have_received(:_determine_agent_current_state).with(agent: agent)
      end

      it 'does not call #_determine_agent_desired_state' do
        test_instance.__send__(:_determine_agent_state, agent: agent)

        expect(test_instance).not_to have_received(:_determine_agent_desired_state)
      end

      it 'returns current state' do
        expect(test_instance.__send__(:_determine_agent_state, agent: agent)).to eq(current_state)
      end
    end

    context 'without current state' do
      let(:current_state) { nil }

      it 'calls #_determine_agent_current_state' do
        test_instance.__send__(:_determine_agent_state, agent: agent)

        expect(test_instance).to have_received(:_determine_agent_current_state).with(agent: agent)
      end

      it 'calls #_determine_agent_desired_state' do
        test_instance.__send__(:_determine_agent_state, agent: agent)

        expect(test_instance).to have_received(:_determine_agent_desired_state).with(agent: agent)
      end

      it 'returns desired state' do
        expect(test_instance.__send__(:_determine_agent_state, agent: agent)).to eq(desired_state)
      end
    end
  end

  describe '#_determine_agent_current_state' do
    let(:agent) { instance_double(Dipa::Agent) }
    let(:state) { Dipa::Agent::COMBINED_FINISHED_STATES.except(:finished).values.sample }

    before do
      allow(agent).to receive_messages(
        reload: agent,
        failed_state?: failed_state?,
        state: state
      )
    end

    context 'with failed state' do
      let(:failed_state?) { true }

      it 'reloads agent' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).to have_received(:reload)
      end

      it 'calls #failed_state? on agent instance' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).to have_received(:failed_state?)
      end

      it 'calls #state on agent instance' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).to have_received(:state)
      end

      it 'returns agents state' do
        expect(test_instance.__send__(:_determine_agent_current_state, agent: agent)).to eq(state.to_sym)
      end
    end

    context 'without failed state' do
      let(:failed_state?) { false }

      it 'reloads agent' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).to have_received(:reload)
      end

      it 'calls #failed_state? on agent instance' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).to have_received(:failed_state?)
      end

      it 'does not call #state on agent instance' do
        test_instance.__send__(:_determine_agent_current_state, agent: agent)

        expect(agent).not_to have_received(:state)
      end

      it 'returns nil' do
        expect(test_instance.__send__(:_determine_agent_current_state, agent: agent)).to be_nil
      end
    end
  end

  describe '#_determine_agent_desired_state' do
    let(:agent) { create(:dipa_agent) }

    before do
      Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
        allow(test_instance).to receive("_#{state}_state_desired?".to_sym).and_return(state == desired_state)
      end
    end

    shared_examples 'it determines agent state' do |state|
      let(:desired_state) { state }

      state_index = Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.index(state)

      Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each_with_index do |test_state, i|
        expect_call = i <= state_index

        it "#{expect_call ? 'calls' : 'does not call'} #_#{test_state}_state_desired?" do
          test_instance.__send__(:_determine_agent_desired_state, agent: agent)

          expect(test_instance).__send__(
            expect_call ? :to : :not_to, have_received("_#{test_state}_state_desired?".to_sym).with(agent: agent)
          )
        end
      end

      it 'returns agent state' do
        expect(test_instance.__send__(:_determine_agent_desired_state, agent: agent)).to eq(state)
      end
    end

    Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
      it_behaves_like 'it determines agent state', state
    end
  end

  describe '#_aborted_through_coordinator_state_desired?' do
    let(:coordinator) { create(:dipa_coordinator) }
    let(:agent) { instance_double(Dipa::Agent, coordinator: coordinator) }

    before do
      allow(agent).to receive(:reload).and_return(agent)
      allow(coordinator).to receive(:failed_state?).and_return([true, false].sample)
    end

    it 'reloads agent' do
      test_instance.__send__(:_aborted_through_coordinator_state_desired?, agent: agent)

      expect(agent).to have_received(:reload)
    end

    it 'calls #failed_state? on coordinator instance' do
      test_instance.__send__(:_aborted_through_coordinator_state_desired?, agent: agent)

      expect(coordinator).to have_received(:failed_state?)
    end
  end

  describe '#_aborted_through_coordinator_timed_out_state_desired?' do
    let(:coordinator) { create(:dipa_coordinator) }
    let(:agent) { instance_double(Dipa::Agent, coordinator: coordinator) }

    before do
      allow(agent).to receive(:reload).and_return(agent)
      allow(coordinator).to receive(:timeout_reached?).and_return([true, false].sample)
    end

    it 'reloads agent' do
      test_instance.__send__(:_aborted_through_coordinator_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:reload)
    end

    it 'calls #timeout_reached? on agent instance' do
      test_instance.__send__(:_aborted_through_coordinator_timed_out_state_desired?, agent: agent)

      expect(coordinator).to have_received(:timeout_reached?)
    end
  end

  describe '#_aborted_through_coordinator_processing_timed_out_state_desired?' do
    let(:coordinator) { create(:dipa_coordinator) }
    let(:agent) { instance_double(Dipa::Agent, coordinator: coordinator) }

    before do
      allow(agent).to receive(:reload).and_return(agent)
      allow(coordinator).to receive(:processing_timeout_reached?).and_return([true, false].sample)
    end

    it 'reloads agent' do
      test_instance.__send__(:_aborted_through_coordinator_processing_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:reload)
    end

    it 'calls #processing_timeout_reached? on agent instance' do
      test_instance.__send__(:_aborted_through_coordinator_processing_timed_out_state_desired?, agent: agent)

      expect(coordinator).to have_received(:processing_timeout_reached?)
    end
  end

  describe '#_timed_out_state_desired?' do
    let(:agent) { instance_double(Dipa::Agent) }

    before do
      allow(agent).to receive_messages(
        reload: agent,
        timeout_reached?: [true, false].sample
      )
    end

    it 'reloads agent' do
      test_instance.__send__(:_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:reload)
    end

    it 'calls #timeout_reached? on agent instance' do
      test_instance.__send__(:_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:timeout_reached?)
    end
  end

  describe '#_processing_timed_out_state_desired?' do
    let(:agent) { instance_double(Dipa::Agent) }

    before do
      allow(agent).to receive_messages(
        reload: agent,
        processing_timeout_reached?: [true, false].sample
      )
    end

    it 'reloads agent' do
      test_instance.__send__(:_processing_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:reload)
    end

    it 'calls #processing_timeout_reached? on agent instance' do
      test_instance.__send__(:_processing_timed_out_state_desired?, agent: agent)

      expect(agent).to have_received(:processing_timeout_reached?)
    end
  end

  describe '#_end_it_all' do
    shared_examples 'it ends all' do |state|
      let(:agent) { instance_double(Dipa::Agent) }
      let(:thread) { instance_double(Thread) }
      let(:error_class_name) { "Dipa::Agent#{state.to_s.camelize}Error" }

      before do
        allow(agent).to receive("#{state}?".to_sym).and_return(state?)
        allow(agent).to receive(:reload).and_return(agent)
        allow(agent).to receive("#{state}!".to_sym)
        allow(thread).to receive(:raise)
      end

      context 'when state ist already set' do
        let(:state?) { true }

        it 'calls #reload on agent instance' do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).to have_received(:reload)
        end

        it "calls ##{state}? on agent instance" do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).to have_received("#{state}?".to_sym)
        end

        it "does not call ##{state}!" do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).not_to have_received("#{state}!".to_sym)
        end

        it 'calls #raise on thread' do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(thread).to have_received(:raise).with(error_class_name.constantize)
        end
      end

      context 'when state ist not set' do
        let(:state?) { false }

        it 'calls #reload on agent instance' do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).to have_received(:reload)
        end

        it "calls ##{state}? on agent instance" do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).to have_received("#{state}?".to_sym)
        end

        it "calls ##{state}!" do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(agent).to have_received("#{state}!".to_sym)
        end

        it 'calls #raise on thread' do
          test_instance.__send__(:_end_it_all!, agent: agent, thread: thread, state: state,
                                                error_class_name: error_class_name)

          expect(thread).to have_received(:raise).with(error_class_name.constantize)
        end
      end
    end

    Dipa::Services::ProcessFailureHandling::STATES_TO_CHECK_FOR.each do |state|
      context "with state `#{state}`" do
        it_behaves_like 'it ends all', state
      end
    end
  end
end
