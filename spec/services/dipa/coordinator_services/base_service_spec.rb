# frozen_string_literal: true

RSpec.describe Dipa::CoordinatorServices::BaseService do
  describe '#call' do
    let(:test_instance) { described_class.new }
    let(:coordinator) { create(:dipa_coordinator) }

    it 'sets @_coordinator' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance.instance_variable_get(:@_coordinator)).to eq(coordinator)
    end
  end

  describe '#initialize' do
    it 'sets @_coordinator' do
      test_instance = described_class.new

      expect(test_instance.instance_variable_get(:@_coordinator)).to be_nil
    end
  end
end
