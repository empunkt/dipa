# frozen_string_literal: true

RSpec.describe Dipa::CoordinatorServices::StartProcessingService do
  describe '#call' do
    let(:coordinator_queue) { 'ze_coordinator_queue' }
    let(:coordinator) do
      create(:dipa_coordinator, coordinator_queue: coordinator_queue)
    end
    let(:test_instance) { described_class.new }
    let(:configured_job) { instance_double(ActiveJob::ConfiguredJob) }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(coordinator)
      allow(Dipa::ServiceJob).to receive(:set).and_return(configured_job)
      allow(configured_job).to receive(:perform_later)
      allow(coordinator).to receive(:processing!)
    end

    it 'calls #_coordinator three times' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance).to have_received(:_coordinator).exactly(3).times
    end

    it 'calls #processing! on coordinator instance' do
      test_instance.call(coordinator: coordinator)

      expect(coordinator).to have_received(:processing!)
    end

    it 'calls Dipa::ServiceJob.set' do
      test_instance.call(coordinator: coordinator)

      expect(Dipa::ServiceJob).to have_received(:set)
        .with(queue: coordinator_queue)
    end

    it 'calls #perform_later on configured job' do
      test_instance.call(coordinator: coordinator)

      expect(configured_job).to have_received(:perform_later)
        .with(service_class_name:
                'Dipa::CoordinatorServices::CreateAgentsService',
              kwargs: { coordinator: coordinator })
    end
  end
end
