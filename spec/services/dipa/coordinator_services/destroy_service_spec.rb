# frozen_string_literal: true

RSpec.describe Dipa::CoordinatorServices::DestroyService do
  describe '#call' do
    let(:test_instance) { described_class.new }

    context 'with coordinator' do
      it 'removes coordinator from database' do
        coordinator = create(:dipa_coordinator)

        expect { test_instance.call(coordinator: coordinator) }.to change(Dipa::Coordinator, :count).by(-1)
      end
    end

    context 'without coordinator' do
      it 'does nothing' do
        coordinator = build(:dipa_coordinator)

        expect { test_instance.call(coordinator: coordinator) }.not_to change(Dipa::Coordinator, :count)
      end
    end
  end
end
