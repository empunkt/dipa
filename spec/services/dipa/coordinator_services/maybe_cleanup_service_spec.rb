# frozen_string_literal: true

RSpec.describe Dipa::CoordinatorServices::MaybeCleanupService do
  describe '#call' do
    let(:coordinator_queue) { 'ze_coordinator_queue' }
    let(:coordinator) do
      create(:dipa_coordinator, coordinator_queue: coordinator_queue)
    end
    let(:test_instance) { described_class.new }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(coordinator)
      allow(coordinator).to receive(:keep_data?).and_return(keep_data?)
    end

    context 'when #keep_data? returns true' do
      let(:keep_data?) { true }

      before do
        allow(Dipa::ServiceJob).to receive(:set)
      end

      it 'calls #_coordinator once' do
        test_instance.call(coordinator: coordinator)

        expect(test_instance).to have_received(:_coordinator).once
      end

      it 'calls #keep_data? on coordinator instance' do
        test_instance.call(coordinator: coordinator)

        expect(coordinator).to have_received(:keep_data?)
      end

      it 'does not call Dipa::ServiceJob.set' do
        test_instance.call(coordinator: coordinator)

        expect(Dipa::ServiceJob).not_to have_received(:set)
      end
    end

    context 'when #keep_data? returns false' do
      let(:keep_data?) { false }
      let(:configured_job) { instance_double(ActiveJob::ConfiguredJob) }

      before do
        allow(Dipa::ServiceJob).to receive(:set).and_return(configured_job)
        allow(configured_job).to receive(:perform_later)
      end

      it 'calls #_coordinator three times' do
        test_instance.call(coordinator: coordinator)

        expect(test_instance).to have_received(:_coordinator).exactly(3).times
      end

      it 'calls #keep_data? on coordinator instance' do
        test_instance.call(coordinator: coordinator)

        expect(coordinator).to have_received(:keep_data?)
      end

      it 'calls Dipa::ServiceJob.set' do
        test_instance.call(coordinator: coordinator)

        expect(Dipa::ServiceJob).to have_received(:set)
          .with(queue: coordinator_queue)
      end

      it 'calls #perform_later on configured job' do
        test_instance.call(coordinator: coordinator)

        expect(configured_job).to have_received(:perform_later)
          .with(service_class_name:
                  'Dipa::CoordinatorServices::DestroyService',
                kwargs: { coordinator: coordinator })
      end
    end
  end
end
