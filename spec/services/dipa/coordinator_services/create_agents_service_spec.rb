# frozen_string_literal: true

RSpec.describe Dipa::CoordinatorServices::CreateAgentsService do
  describe '#call' do
    let(:test_instance) { described_class.new }
    let(:coordinator) { create(:dipa_coordinator) }
    let(:source) { [anything, anything] }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(coordinator)
      allow(coordinator).to receive(:source).and_return(source)
      allow(test_instance).to receive(:_create_agent)
    end

    it 'calls #_coordinator' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance).to have_received(:_coordinator)
    end

    it 'calls #source on coordinator instance' do
      test_instance.call(coordinator: coordinator)

      expect(coordinator).to have_received(:source)
    end

    it 'calls #_create_agent for every item' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance).to have_received(:_create_agent)
        .exactly(source.size).times
    end

    it 'calls #_create_agent for first item' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance).to have_received(:_create_agent).with(item: source[0], index: 0)
    end

    it 'calls #_create_agent for second item' do
      test_instance.call(coordinator: coordinator)

      expect(test_instance).to have_received(:_create_agent).with(item: source[1], index: 1)
    end
  end

  describe '#_create_agent' do
    let(:test_instance) { described_class.new }
    let(:coordinator) { create(:dipa_coordinator) }
    let(:agent) { build(:dipa_agent) }
    let(:item) { anything }
    let(:index) { 23 }

    before do
      allow(test_instance).to receive(:_coordinator).and_return(coordinator)
      allow(coordinator.agents).to receive(:create!).and_return(agent)
      allow(agent).to receive(:dump_to_file)
      allow(Dipa::AgentServices::StartProcessingService).to receive(:call)
    end

    it 'calls #_coordinator' do
      test_instance.send(:_create_agent, item: item, index: index)

      expect(test_instance).to have_received(:_coordinator)
    end

    it 'calls #create! on coordinators agents association' do
      test_instance.send(:_create_agent, item: item, index: index)

      expect(coordinator.agents).to have_received(:create!).with(index: index)
    end

    it 'calls #dump_to_file on created agent' do
      test_instance.send(:_create_agent, item: item, index: index)

      expect(agent).to have_received(:dump_to_file)
        .with(data: item, attacher: :source_dump)
    end

    it 'calls Dipa::AgentServices::StartProcessingService.call' do
      test_instance.send(:_create_agent, item: item, index: index)

      expect(
        Dipa::AgentServices::StartProcessingService
      ).to have_received(:call).with(agent: agent)
    end
  end
end
