# frozen_string_literal: true

RSpec.describe Dipa::AgentServices::BaseService do
  describe '#call' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }

    it 'sets @_agent' do
      test_instance.call(agent: agent)

      expect(test_instance.instance_variable_get(:@_agent)).to eq(agent)
    end
  end

  describe '#initialize' do
    it 'sets @_agent' do
      test_instance = described_class.new

      expect(test_instance.instance_variable_get(:@_agent)).to be_nil
    end
  end
end
