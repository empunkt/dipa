# frozen_string_literal: true

RSpec.describe Dipa::AgentServices::PostProcessingService do
  describe '#call' do
    let(:agent_queue) { 'ze_agent_queue' }
    let(:agent) do
      create(
        :dipa_agent,
        coordinator: create(
          :dipa_coordinator,
          agent_queue: agent_queue
        )
      )
    end
    let(:test_instance) { described_class.new }
    let(:configured_job) { instance_double(ActiveJob::ConfiguredJob) }

    before do
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(Dipa::ServiceJob).to receive(:set).and_return(configured_job)
      allow(configured_job).to receive(:perform_later)
    end

    it 'calls #_agent twice' do
      test_instance.call(agent: agent)

      expect(test_instance).to have_received(:_agent).twice
    end

    it 'calls Dipa::ServiceJob.set' do
      test_instance.call(agent: agent)

      expect(Dipa::ServiceJob).to have_received(:set)
        .with(queue: agent_queue)
    end

    it 'calls #perform_later on configured job' do
      test_instance.call(agent: agent)

      expect(configured_job).to have_received(:perform_later)
        .with(service_class_name:
                'Dipa::AgentServices::CoordinatorStateService',
              kwargs: { agent: agent })
    end
  end
end
