# frozen_string_literal: true

RSpec.describe Dipa::AgentServices::ProcessingService do
  it_behaves_like 'it handles process failures'

  describe '#call' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }

    before do
      allow(test_instance).to receive(:_post_processing)
    end

    context 'when processing succeeds' do
      before do
        allow(test_instance).to receive(:_process!)
      end

      it 'calls #process! on agent instance' do
        test_instance.call(agent: agent)

        expect(test_instance).to have_received(:_process!)
      end

      it 'calls #_post_processing on agent instance' do
        test_instance.call(agent: agent)

        expect(test_instance).to have_received(:_post_processing)
      end
    end

    context 'when processing fails' do
      before do
        allow(test_instance).to receive(:_process!).and_raise
      end

      it 'calls #process! on agent instance' do
        test_instance.call(agent: agent)
      rescue StandardError
        expect(test_instance).to have_received(:_process!)
      end

      it 'calls #_post_processing on agent instance' do
        test_instance.call(agent: agent)
      rescue StandardError
        expect(test_instance).to have_received(:_post_processing)
      end
    end
  end

  describe '#initialize' do
    it 'sets @_processor_thread' do
      test_instance = described_class.new

      expect(test_instance.instance_variable_get(:@_processor_thread)).to eq(Thread.current)
    end
  end

  describe '#_process!' do
    let(:test_instance) { described_class.new }
    let(:processor_result) { anything }
    let(:agent) { instance_double(Dipa::Agent) }

    before do
      allow(test_instance).to receive(:_start_monitoring_task)
      allow(test_instance).to receive_messages(_processor_result: processor_result, _agent: agent)
      allow(agent).to receive(:dump_to_file)
      allow(agent).to receive(:finished!)
      allow(agent).to receive(:processing_failed!)
    end

    it 'calls #_start_monitoring_task' do
      test_instance.__send__(:_process!)

      expect(test_instance).to have_received(:_start_monitoring_task)
    end

    it 'calls #_processor_result' do
      test_instance.__send__(:_process!)

      expect(test_instance).to have_received(:_processor_result)
    end

    it 'calls #dump_to_file on agent instance' do
      test_instance.__send__(:_process!)

      expect(agent).to have_received(:dump_to_file).with(data: processor_result, attacher: :result_dump)
    end

    it 'calls #finished! on agent instance' do
      test_instance.__send__(:_process!)

      expect(agent).to have_received(:finished!)
    end

    it 'does not call #processing_failed! on agent instance' do
      test_instance.__send__(:_process!)

      expect(agent).not_to have_received(:processing_failed!)
    end

    context 'when an error raises' do
      context 'with non Dipa::AgentError' do
        before do
          allow(test_instance).to receive(:_start_monitoring_task).and_raise(StandardError)
        end

        it 'calls #processing_failed! on agent instance' do
          test_instance.__send__(:_process!)

          expect(agent).to have_received(:processing_failed!)
        end
      end

      context 'with Dipa::AgentError' do
        before do
          allow(test_instance).to receive(:_start_monitoring_task).and_raise(Dipa::AgentError)
        end

        it 'does not call #processing_failed! on agent instance' do
          test_instance.__send__(:_process!)

          expect(agent).not_to have_received(:processing_failed!)
        end
      end
    end
  end

  describe '#_processor_result' do
    let(:test_instance) { described_class.new }
    let(:processor_class_name) { SecureRandom.__send__(:choose, [*'a'..'z'], 16).upcase_first }
    let(:processor_method_name) { SecureRandom.__send__(:choose, [*'a'..'z'], 16) }
    let(:processor_class) do
      klass = Class.new
      # rubocop:disable Style/DocumentDynamicEvalDefinition
      klass.class_eval("def self.#{processor_method_name}(source); end", __FILE__, __LINE__)
      # rubocop:enable Style/DocumentDynamicEvalDefinition
      klass
    end
    let(:source) { anything }
    let(:agent) do
      create(
        :dipa_agent,
        coordinator: build(
          :dipa_coordinator,
          processor_class_name: processor_class_name,
          processor_method_name: processor_method_name
        )
      )
    end
    let(:coordinator) { agent.coordinator }
    let(:result) { anything }

    before do
      processor_class
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(processor_class).to receive(processor_method_name.to_sym).and_return(result)
      Object.const_set(processor_class_name, processor_class)
    end

    after do
      Object.__send__(:remove_const, processor_class_name) if defined?(processor_class_name)
    end

    it 'calls #_agent three times' do
      test_instance.__send__(:_processor_result)

      expect(test_instance).to have_received(:_agent).exactly(3).times
    end

    it 'calls processor method on processor class' do
      test_instance.__send__(:_processor_result)

      expect(processor_class).to have_received(processor_method_name.to_sym).with(source)
    end

    it 'returns result' do
      expect(test_instance.__send__(:_processor_result)).to eq(result)
    end
  end

  describe '#_monitoring_task' do
    let(:test_instance) { described_class.new }
    let(:task) { instance_double(Concurrent::TimerTask) }
    let(:iteration_proc) { proc {} }

    before do
      allow(Concurrent::TimerTask).to receive(:new).and_return(task)
      allow(test_instance).to receive(:_monitoring_task_iteration_block).and_return(iteration_proc)
    end

    it 'calls #_monitoring_task_iteration_block' do
      test_instance.__send__(:_monitoring_task)

      expect(test_instance).to have_received(:_monitoring_task_iteration_block)
    end

    it 'initializes monitoring task' do
      test_instance.__send__(:_monitoring_task)

      expect(Concurrent::TimerTask).to have_received(:new).with(described_class::TIMER_TASK_ARGS, &iteration_proc)
    end

    it 'returns monitoring task instance' do
      expect(test_instance.__send__(:_monitoring_task)).to eq(task)
    end

    it 'is lazy' do
      test_instance.__send__(:_monitoring_task)
      test_instance.__send__(:_monitoring_task)

      expect(Concurrent::TimerTask).to have_received(:new).once
    end
  end

  describe '#_monitoring_task_iteration_block' do
    let(:test_instance) { described_class.new }
    let(:task) { instance_double(Concurrent::TimerTask) }

    before do
      allow(test_instance).to receive(:_monitoring_task_iteration)
    end

    it 'returns an Proc instance' do
      expect(test_instance.__send__(:_monitoring_task_iteration_block)).to be_a(Proc)
    end

    it 'calls #_monitoring_task_iteration when called' do
      test_instance.__send__(:_monitoring_task_iteration_block).call(task)

      expect(test_instance).to have_received(:_monitoring_task_iteration).with(task: task)
    end
  end

  describe '#_monitoring_task_iteration' do
    let(:test_instance) { described_class.new }
    let(:task) { instance_double(Concurrent::TimerTask) }
    let(:executor) { class_double(ActiveSupport::Executor) }
    let(:execution_context) { instance_double(ActiveSupport::Executor) }
    let(:agent) { instance_double(Dipa::Agent) }
    let(:processor_thread) { instance_double(Thread) }

    before do
      allow(Rails.application).to receive(:executor).and_return(executor)
      allow(executor).to receive(:run!).and_return(execution_context)
      allow(test_instance).to receive(:_handle_execution_interval)
      allow(test_instance).to receive_messages(_agent: agent, _stop_processing?: stop_processing?,
                                               _processor_thread: processor_thread)
      allow(test_instance).to receive(:_stop_processing!)
      allow(task).to receive(:shutdown)
      allow(execution_context).to receive(:complete!)
    end

    shared_examples 'it does common task iteration stuff' do
      it 'calls Rails.application.executor' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(Rails.application).to have_received(:executor)
      end

      it 'starts execution context' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(executor).to have_received(:run!)
      end

      it 'calls #_handle_execution_interval' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_handle_execution_interval).with(task: task)
      end

      it 'calls #_stop_processing?' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_stop_processing?).with(agent: agent)
      end

      it 'completes execution context' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(execution_context).to have_received(:complete!)
      end
    end

    context 'when processing should be stopped' do
      let(:stop_processing?) { true }

      it_behaves_like 'it does common task iteration stuff'

      it 'calls #_agent twice' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_agent).twice
      end

      it 'calls #_processor_thread' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_processor_thread)
      end

      it 'calls #_stop_processing!' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_stop_processing!).with(agent: agent, thread: processor_thread)
      end

      it 'shuts down timer task' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(task).to have_received(:shutdown)
      end
    end

    context 'when processing should not be stopped' do
      let(:stop_processing?) { false }

      it_behaves_like 'it does common task iteration stuff'

      it 'calls #_agent once' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).to have_received(:_agent).once
      end

      it 'does not call #_processor_thread' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).not_to have_received(:_processor_thread)
      end

      it 'does not call #_stop_processing!' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(test_instance).not_to have_received(:_stop_processing!)
      end

      it 'does not shut down timer task' do
        test_instance.__send__(:_monitoring_task_iteration, task: task)

        expect(task).not_to have_received(:shutdown)
      end
    end
  end

  describe '#_handle_execution_interval' do
    let(:test_instance) { described_class.new }
    let(:execution_interval) { instance_double(Float) }
    let(:task) { instance_double(Concurrent::TimerTask, execution_interval: execution_interval) }

    before do
      allow(test_instance).to receive(:_max_execution_interval_reached?).and_return(max_execution_interval_reached?)
      allow(task).to receive(:'execution_interval=')
      allow(execution_interval).to receive(:*)
    end

    context 'when max execution interval is reached' do
      let(:max_execution_interval_reached?) { true }

      it 'calls #_max_execution_interval_reached?' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(test_instance).to have_received(:_max_execution_interval_reached?)
      end

      it 'does not increase execution interval' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(execution_interval).not_to have_received(:*)
      end

      it 'does not set new execution interval' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(task).not_to have_received(:'execution_interval=')
      end
    end

    context 'when max execution interval is not reached' do
      let(:max_execution_interval_reached?) { false }

      it 'calls #_max_execution_interval_reached?' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(test_instance).to have_received(:_max_execution_interval_reached?)
      end

      it 'increases execution interval' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(execution_interval).to have_received(:*).with(described_class::TIMER_TASK_SLEEP_DURATION_INCREASE_FACTOR)
      end

      it 'sets new execution interval' do
        test_instance.__send__(:_handle_execution_interval, task: task)

        expect(task).to have_received(:'execution_interval=').with(
          execution_interval * described_class::TIMER_TASK_SLEEP_DURATION_INCREASE_FACTOR
        )
      end
    end
  end

  describe '#_max_execution_interval_reached?' do
    let(:test_instance) { described_class.new }
    let(:task) { instance_double(Concurrent::TimerTask, execution_interval: execution_interval) }
    let(:max_sleep_duration) { 23 }

    before do
      stub_const("#{described_class}::TIMER_TASK_MAX_SLEEP_DURATION", max_sleep_duration)
    end

    context 'when execution interval is greater than TIMER_TASK_MAX_SLEEP_DURATION' do
      let(:execution_interval) { 24 }

      it 'returns true' do
        expect(test_instance.__send__(:_max_execution_interval_reached?, task: task)).to be_truthy
      end
    end

    context 'when execution interval is not greater than TIMER_TASK_MAX_SLEEP_DURATION' do
      let(:execution_interval) { 23 }

      it 'returns false' do
        expect(test_instance.__send__(:_max_execution_interval_reached?, task: task)).to be_falsey
      end
    end
  end

  describe '#_start_monitoring_task' do
    let(:test_instance) { described_class.new }
    let(:monitoring_task) { instance_double(Concurrent::TimerTask) }

    before do
      allow(test_instance).to receive(:_monitoring_task).and_return(monitoring_task)
      allow(monitoring_task).to receive(:execute)
    end

    it 'calls #_monitoring_task' do
      test_instance.__send__(:_start_monitoring_task)

      expect(test_instance).to have_received(:_monitoring_task)
    end

    it 'calls #execute' do
      test_instance.__send__(:_start_monitoring_task)

      expect(monitoring_task).to have_received(:execute)
    end
  end

  describe '#_post_processing' do
    let(:test_instance) { described_class.new }
    let(:monitoring_task) { instance_double(Concurrent::TimerTask) }
    let(:agent) { instance_double(Dipa::Agent) }

    before do
      allow(test_instance).to receive_messages(_monitoring_task: monitoring_task, _agent: agent)
      allow(monitoring_task).to receive(:shutdown)
      allow(monitoring_task).to receive(:running?).and_return(running?)
      allow(Dipa::AgentServices::PostProcessingService).to receive(:call)
    end

    context 'when monitoring task is running' do
      let(:running?) { true }

      it 'calls #_monitoring_task twice' do
        test_instance.__send__(:_post_processing)

        expect(test_instance).to have_received(:_monitoring_task).twice
      end

      it 'calls #running? on monitoring task' do
        test_instance.__send__(:_post_processing)

        expect(monitoring_task).to have_received(:running?)
      end

      it 'calls #shutdown on monitoring task' do
        test_instance.__send__(:_post_processing)

        expect(monitoring_task).to have_received(:shutdown)
      end

      it 'calls #_agent' do
        test_instance.__send__(:_post_processing)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls Dipa::AgentServices::PostProcessingService.call' do
        test_instance.__send__(:_post_processing)

        expect(Dipa::AgentServices::PostProcessingService).to have_received(:call).with(agent: agent)
      end
    end

    context 'when monitoring task is not running' do
      let(:running?) { false }

      it 'calls #_monitoring_task' do
        test_instance.__send__(:_post_processing)

        expect(test_instance).to have_received(:_monitoring_task)
      end

      it 'calls #running? on monitoring task' do
        test_instance.__send__(:_post_processing)

        expect(monitoring_task).to have_received(:running?)
      end

      it 'does not call #shutdown on monitoring task' do
        test_instance.__send__(:_post_processing)

        expect(monitoring_task).not_to have_received(:shutdown)
      end

      it 'calls #_agent' do
        test_instance.__send__(:_post_processing)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls Dipa::AgentServices::PostProcessingService.call' do
        test_instance.__send__(:_post_processing)

        expect(Dipa::AgentServices::PostProcessingService).to have_received(:call).with(agent: agent)
      end
    end
  end
end
