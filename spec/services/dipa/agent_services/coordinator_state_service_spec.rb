# frozen_string_literal: true

RSpec.describe Dipa::AgentServices::CoordinatorStateService do
  describe '#call' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }
    let(:coordinator) { agent.coordinator }

    before do
      allow(agent).to receive(:reload).and_return(agent)
      allow(coordinator).to receive(:finished_state?).and_return(finished_state?)
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(test_instance).to receive(:_handle_agent_states)
    end

    context 'when coordinator does not have finished state' do
      let(:finished_state?) { false }

      it 'calls #_agent' do
        test_instance.call(agent: agent)

        expect(test_instance).to have_received(:_agent)
      end

      it 'reloads agent' do
        test_instance.call(agent: agent)

        expect(agent).to have_received(:reload)
      end

      it 'calls #finished_state? on coordinator instance' do
        test_instance.call(agent: agent)

        expect(coordinator).to have_received(:finished_state?)
      end

      it 'calls #_handle_agent_states' do
        test_instance.call(agent: agent)

        expect(test_instance).to have_received(:_handle_agent_states)
      end
    end

    context 'when coordinator has finished state' do
      let(:finished_state?) { true }

      it 'calls #_agent' do
        test_instance.call(agent: agent)

        expect(test_instance).to have_received(:_agent)
      end

      it 'reloads agent' do
        test_instance.call(agent: agent)

        expect(agent).to have_received(:reload)
      end

      it 'calls #finished_state? on coordinator instance' do
        test_instance.call(agent: agent)

        expect(coordinator).to have_received(:finished_state?)
      end

      it 'does not call #_handle_agent_states' do
        test_instance.call(agent: agent)

        expect(test_instance).not_to have_received(:_handle_agent_states)
      end
    end
  end

  describe '#_handle_agent_states' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }

    before do
      allow(agent).to receive(:failed_state?).and_return(failed_state?)
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(test_instance).to receive(:_handle_agent_failed_states)
      allow(test_instance).to receive(:_handle_agent_success_states)
    end

    context 'when agent has failed state' do
      let(:failed_state?) { true }

      it 'calls #_agent' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls #failed_state? on agent instance' do
        test_instance.__send__(:_handle_agent_states)

        expect(agent).to have_received(:failed_state?)
      end

      it 'calls #_handle_agent_failed_states' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).to have_received(:_handle_agent_failed_states)
      end

      it 'does not call #_handle_agent_success_states' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).not_to have_received(:_handle_agent_success_states)
      end
    end

    context 'when agent does not have failed state' do
      let(:failed_state?) { false }

      it 'calls #_agent' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls #be_failed_state? on agent instance' do
        test_instance.__send__(:_handle_agent_states)

        expect(agent).to have_received(:failed_state?)
      end

      it 'does not call #_handle_agent_failed_states' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).not_to have_received(:_handle_agent_failed_states)
      end

      it 'calls #_handle_agent_success_states' do
        test_instance.__send__(:_handle_agent_states)

        expect(test_instance).to have_received(:_handle_agent_success_states)
      end
    end
  end

  describe '#_determine_coordinator_failed_state' do
    let(:test_instance) { described_class.new }

    before do
      allow(test_instance).to receive_messages(
        _determine_aborted_through_coordinator_state: aborted_through_coordinator_state
      )
    end

    context 'when aborted_through_coordinator_state is not nil' do
      let(:aborted_through_coordinator_state) { instance_double(Symbol) }

      before do
        allow(test_instance).to receive(:_determine_aborted_through_agent_state)
      end

      it 'calls #_determine_aborted_through_coordinator_state' do
        test_instance.__send__(:_determine_coordinator_failed_state)

        expect(test_instance).to have_received(:_determine_aborted_through_coordinator_state)
      end

      it 'does not call #_determine_aborted_through_agent_state' do
        test_instance.__send__(:_determine_coordinator_failed_state)

        expect(test_instance).not_to have_received(:_determine_aborted_through_agent_state)
      end

      it 'returns result from #_determine_aborted_through_coordinator_state' do
        expect(
          test_instance.__send__(:_determine_coordinator_failed_state)
        ).to eq(aborted_through_coordinator_state)
      end
    end

    context 'when aborted_through_coordinator_state is nil' do
      let(:aborted_through_coordinator_state) { nil }

      before do
        allow(test_instance).to receive(:_determine_aborted_through_agent_state).and_return(aborted_through_agent_state)
      end

      context 'when aborted_through_agent_state is not nil' do
        let(:aborted_through_agent_state) { instance_double(Symbol) }

        it 'calls #_determine_aborted_through_coordinator_state' do
          test_instance.__send__(:_determine_coordinator_failed_state)

          expect(test_instance).to have_received(:_determine_aborted_through_coordinator_state)
        end

        it 'calls #_determine_aborted_through_agent_state' do
          test_instance.__send__(:_determine_coordinator_failed_state)

          expect(test_instance).to have_received(:_determine_aborted_through_agent_state)
        end

        it 'returns result from #_determine_aborted_through_agent_state' do
          expect(
            test_instance.__send__(:_determine_coordinator_failed_state)
          ).to eq(aborted_through_agent_state)
        end
      end

      context 'when aborted_through_agent_state is nil' do
        let(:aborted_through_agent_state) { nil }

        it 'calls #_determine_aborted_through_coordinator_state' do
          test_instance.__send__(:_determine_coordinator_failed_state)

          expect(test_instance).to have_received(:_determine_aborted_through_coordinator_state)
        end

        it 'calls #_determine_aborted_through_agent_state' do
          test_instance.__send__(:_determine_coordinator_failed_state)

          expect(test_instance).to have_received(:_determine_aborted_through_agent_state)
        end

        it 'returns :aborted_through_agent' do
          expect(
            test_instance.__send__(:_determine_coordinator_failed_state)
          ).to eq(:aborted_through_agent)
        end
      end
    end
  end

  describe '#_determine_aborted_through_coordinator_state' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }

    before do
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(agent).to receive_messages(
        state: state.to_s,
        reload: agent
      )
    end

    context 'when state is not in COORDINATOR_STATES_TO_CHECK_FOR' do
      let(:state) { :foo }

      it 'calls #_agent' do
        test_instance.__send__(:_determine_aborted_through_coordinator_state)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls #reload on agent instance' do
        test_instance.__send__(:_determine_aborted_through_coordinator_state)

        expect(agent).to have_received(:reload)
      end

      it 'calls #state on agent instance' do
        test_instance.__send__(:_determine_aborted_through_coordinator_state)

        expect(agent).to have_received(:state)
      end

      it 'returns nil' do
        expect(
          test_instance.__send__(:_determine_aborted_through_coordinator_state)
        ).to be_nil
      end
    end

    context 'when state is in COORDINATOR_STATES_TO_CHECK_FOR' do
      shared_examples 'it handles state to check for' do |state_to_check_for|
        let(:state) { state_to_check_for }

        it 'calls #_agent' do
          test_instance.__send__(:_determine_aborted_through_coordinator_state)

          expect(test_instance).to have_received(:_agent)
        end

        it 'calls #reload on agent instance' do
          test_instance.__send__(:_determine_aborted_through_coordinator_state)

          expect(agent).to have_received(:reload)
        end

        it 'calls #state on agent instance' do
          test_instance.__send__(:_determine_aborted_through_coordinator_state)

          expect(agent).to have_received(:state)
        end

        it 'returns coordinator desired failed state' do
          expect(
            test_instance.__send__(:_determine_aborted_through_coordinator_state)
          ).to eq(state.to_s.delete_prefix('aborted_through_coordinator_').to_sym)
        end
      end

      described_class::COORDINATOR_STATES_TO_CHECK_FOR.each do |state|
        it_behaves_like 'it handles state to check for', state
      end
    end
  end

  describe '#_determine_aborted_through_agent_state' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }

    before do
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(agent).to receive_messages(
        state: state,
        reload: agent
      )
    end

    context 'when state is not in AGENT_STATES_TO_CHECK_FOR' do
      let(:state) { :foo }

      it 'calls #_agent' do
        test_instance.__send__(:_determine_aborted_through_agent_state)

        expect(test_instance).to have_received(:_agent)
      end

      it 'calls #reload on agent instance' do
        test_instance.__send__(:_determine_aborted_through_agent_state)

        expect(agent).to have_received(:reload)
      end

      it 'calls #state on agent instance' do
        test_instance.__send__(:_determine_aborted_through_agent_state)

        expect(agent).to have_received(:state)
      end

      it 'returns nil' do
        expect(
          test_instance.__send__(:_determine_aborted_through_agent_state)
        ).to be_nil
      end
    end

    context 'when state is in AGENT_STATES_TO_CHECK_FOR' do
      shared_examples 'it handles state to chack for' do |state_to_check_for|
        let(:state) { state_to_check_for }

        it 'calls #_agent' do
          test_instance.__send__(:_determine_aborted_through_agent_state)

          expect(test_instance).to have_received(:_agent)
        end

        it 'calls #reload on agent instance' do
          test_instance.__send__(:_determine_aborted_through_agent_state)

          expect(agent).to have_received(:reload)
        end

        it 'calls #state on agent instance' do
          test_instance.__send__(:_determine_aborted_through_agent_state)

          expect(agent).to have_received(:state)
        end

        it 'returns coordinator desired failed state' do
          expect(
            test_instance.__send__(:_determine_aborted_through_agent_state)
          ).to eq("aborted_through_agent_#{state}".to_sym)
        end
      end

      described_class::AGENT_STATES_TO_CHECK_FOR.each do |state|
        it_behaves_like 'it handles state to chack for', state
      end
    end
  end

  describe '#_handle_agent_failed_states' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }
    let(:coordinator) { agent.coordinator }

    before do
      allow(test_instance).to receive_messages(
        _determine_coordinator_failed_state: determined_coordinator_state,
        _agent: agent
      )
      allow(coordinator).to receive(:aborted_through_agent!)
      described_class::AGENT_STATES_TO_CHECK_FOR.each do |state|
        allow(coordinator).to receive("aborted_through_agent_#{state}!".to_sym)
      end
      described_class::COORDINATOR_STATES_TO_CHECK_FOR.each do |state|
        allow(coordinator).to receive("#{state.to_s.delete_prefix('aborted_through_coordinator_')}!".to_sym)
      end
    end

    context 'when determining coordinator state returns state' do
      shared_examples 'it handles failed state' do |state|
        let(:determined_coordinator_state) { state }

        it 'calls #_agent' do
          test_instance.__send__(:_handle_agent_failed_states)

          expect(test_instance).to have_received(:_agent)
        end

        it 'calls #_determine_coordinator_failed_state' do
          test_instance.__send__(:_handle_agent_failed_states)

          expect(test_instance).to have_received(:_determine_coordinator_failed_state)
        end

        it "calls ##{state}! on coordinator instance" do
          test_instance.__send__(:_handle_agent_failed_states)

          expect(coordinator).to have_received("#{state}!".to_sym)
        end
      end

      it_behaves_like 'it handles failed state', :aborted_through_agent

      described_class::COORDINATOR_STATES_TO_CHECK_FOR.each do |state|
        it_behaves_like 'it handles failed state', state.to_s.delete_prefix('aborted_through_coordinator_').to_sym
      end

      described_class::AGENT_STATES_TO_CHECK_FOR.each do |state|
        it_behaves_like 'it handles failed state', "aborted_through_agent_#{state}".to_sym
      end
    end
  end

  describe '#_handle_agent_success_states' do
    let(:test_instance) { described_class.new }
    let(:agent) { create(:dipa_agent) }
    let(:coordinator) { agent.coordinator }

    before do
      allow(agent).to receive(:reload).and_return(agent)
      allow(test_instance).to receive(:_agent).and_return(agent)
      allow(coordinator).to receive(:all_agents_created_and_finished?).and_return(all_agents_created_and_finished?)
      allow(coordinator).to receive(:finished!)
    end

    context 'when all agents created and finished' do
      let(:all_agents_created_and_finished?) { true }

      it 'calls #_agent twice' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(test_instance).to have_received(:_agent).twice
      end

      it 'reloads agent' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(agent).to have_received(:reload)
      end

      it 'calls #all_agents_created_and_finished? on coordinator instance' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(coordinator).to have_received(:all_agents_created_and_finished?)
      end

      it 'calls #finished! on coordinator instance' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(coordinator).to have_received(:finished!)
      end
    end

    context 'when not all agents created and finished' do
      let(:all_agents_created_and_finished?) { false }

      it 'calls #_agent once' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(test_instance).to have_received(:_agent).once
      end

      it 'reloads agent' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(agent).to have_received(:reload)
      end

      it 'calls #all_agents_created_and_finished? on coordinator instance' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(coordinator).to have_received(:all_agents_created_and_finished?)
      end

      it 'does not call #finished! on coordinator instance' do
        test_instance.__send__(:_handle_agent_success_states)

        expect(coordinator).not_to have_received(:finished!)
      end
    end
  end
end
