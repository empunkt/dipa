# frozen_string_literal: true

RSpec.describe Dipa::ApplicationService do
  describe '.call' do
    let(:test_class_name) { 'FooService' }
    let(:test_instance) { test_class.new }
    let(:call_result) { anything }

    before do
      allow(test_class).to receive(:new).and_return(test_instance)
      allow(test_instance).to receive(:call).and_return(call_result)
    end

    after do
      Object.send(:remove_const, test_class_name) if defined?(test_class_name)
    end

    context 'without args and kwargs' do
      let(:test_class) do
        Object.const_set(
          test_class_name,
          Class.new(described_class) do
            def call; end
          end
        )
      end

      it 'calls .new' do
        test_class.call

        expect(test_class).to have_received(:new)
      end

      it 'calls #call' do
        test_class.call

        expect(test_instance).to have_received(:call).with(no_args)
      end

      it 'returns result' do
        expect(test_class.call).to eq(call_result)
      end
    end

    context 'with kwargs' do
      let(:kwargs) { { foo: :bar } }
      let(:test_class) do
        Object.const_set(
          test_class_name,
          Class.new(described_class) do
            def call(**_kwargs); end
          end
        )
      end

      it 'calls .new' do
        test_class.call(**kwargs)

        expect(test_class).to have_received(:new)
      end

      it 'calls #call' do
        test_class.call(**kwargs)

        expect(test_instance).to have_received(:call).with(**kwargs)
      end

      it 'returns result' do
        expect(test_class.call(**kwargs)).to eq(call_result)
      end
    end

    context 'with args' do
      let(:args) { [:foo] }
      let(:test_class) do
        Object.const_set(
          test_class_name,
          Class.new(described_class) do
            def call(*_args); end
          end
        )
      end

      it 'calls .new' do
        test_class.call(*args)

        expect(test_class).to have_received(:new)
      end

      it 'calls #call' do
        test_class.call(*args)

        expect(test_instance).to have_received(:call).with(*args)
      end

      it 'returns result' do
        expect(test_class.call(*args)).to eq(call_result)
      end
    end

    context 'with args and kwargs' do
      let(:args) { [:foo] }
      let(:kwargs) { { foo: :bar } }
      let(:test_class) do
        Object.const_set(
          test_class_name,
          Class.new(described_class) do
            def call(*_args, **_kwargs); end
          end
        )
      end

      it 'calls .new' do
        test_class.call(*args, **kwargs)

        expect(test_class).to have_received(:new)
      end

      it 'calls #call' do
        test_class.call(*args, **kwargs)

        expect(test_instance).to have_received(:call).with(*args, **kwargs)
      end

      it 'returns result' do
        expect(test_class.call(*args, **kwargs)).to eq(call_result)
      end
    end
  end
end
