# frozen_string_literal: true

require 'simplecov'
SimpleCov.start 'rails' do
  enable_coverage :branch
  add_filter 'lib/tasks/auto_annotate_models.rake'
end

# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../spec/dummy/config/environment', __dir__)
# Prevent database truncation if the environment is production

abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this point!

ENV['RBS_TEST_TARGET'] ||= 'Dipa,Dipa::*'
ENV['RBS_TEST_DOUBLE_SUITE'] = 'rspec'
ENV['RBS_TEST_RAISE'] ||= 'true'
ENV['RBS_TEST_LOGLEVEL'] ||= 'warn'
ENV['RBS_TEST_OPT'] ||= "-I sig --collection #{File.expand_path('../rbs_collection.yaml', __dir__)}"
require 'rbs/test/setup'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.

Dir[
  File.join(File.expand_path('..', __dir__), 'spec/support/**/*.rb')
].each { |f| require f }
Dir[
  File.join(File.expand_path('..', __dir__), 'spec/**/concerns/**/*.rb')
].each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  raw_db_config = Rails.configuration.database_configuration
  db_config = ActiveRecord::DatabaseConfigurations.new(raw_db_config)
                                                  .find_db_config(:test)
  db_config = db_config.config if Gem::Version.new(Rails.version) < Gem::Version.new('6.1.0')

  app_migrations_dir = File.expand_path('../spec/dummy/db/migrate', __dir__)
  Dir[File.join(app_migrations_dir, '*.rb')].each { |m| File.unlink(m) }
  migrations_paths = [
    File.expand_path('../db/migrate', __dir__),
    app_migrations_dir
  ]

  include ActiveRecord::Tasks

  DatabaseTasks.env = 'test'
  DatabaseTasks.drop(db_config)
  DatabaseTasks.create(db_config)
  ActiveRecord::Base.establish_connection(db_config)

  Rails.application.load_tasks
  Rake::Task['active_storage:install:migrations'].invoke

  ActiveRecord::Migrator.migrations_paths = migrations_paths

  DatabaseTasks.migrate
  DatabaseTasks.dump_schema(db_config)
  # needed for rails 6.1. See https://github.com/rails/rails/issues/41491
  DatabaseTasks.drop(db_config)
  DatabaseTasks.create(db_config)
  DatabaseTasks.load_schema(db_config)

  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # You can uncomment this line to turn off ActiveRecord support entirely.
  # config.use_active_record = false

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, type: :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  # config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
end
