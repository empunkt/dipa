# frozen_string_literal: true

RSpec.describe Dipa::ServiceJob do
  let(:service_class_name) { 'TestService' }
  let(:args) { [] }
  let(:kwargs) { {} }

  describe 'unit testing' do
    let(:job) { described_class.new }
    let(:arguments) do
      { service_class_name: service_class_name, args: args, kwargs: kwargs }
    end

    before do
      allow(test_class).to receive(:call)
    end

    after do
      if defined?(service_class_name)
        Object.__send__(:remove_const, service_class_name)
      end
    end

    describe '#perform' do
      context 'without args and kwargs' do
        let(:args) { [] }
        let(:kwargs) { {} }
        let(:test_class) do
          Object.const_set(
            service_class_name,
            Class.new do
              def self.call; end
            end
          )
        end

        it 'calls test method on test object' do
          job.perform(**arguments)

          expect(test_class).to have_received(:call).with(no_args)
        end
      end

      context 'with kwargs' do
        let(:args) { [] }
        let(:kwargs) { { foo: :bar } }
        let(:test_class) do
          Object.const_set(
            service_class_name,
            Class.new do
              def self.call(**_kwargs); end
            end
          )
        end

        it 'calls test method on test object' do
          job.perform(**arguments)

          expect(test_class).to have_received(:call).with(**kwargs)
        end
      end

      context 'with args' do
        let(:args) { [:foo] }
        let(:kwargs) { {} }
        let(:test_class) do
          Object.const_set(
            service_class_name,
            Class.new do
              def self.call(*_args); end
            end
          )
        end

        it 'calls test method on test object' do
          job.perform(**arguments)

          expect(test_class).to have_received(:call).with(*args)
        end
      end

      context 'with args and kwargs' do
        let(:args) { [:foo] }
        let(:kwargs) { { foo: :bar } }
        let(:test_class) do
          Object.const_set(
            service_class_name,
            Class.new do
              def self.call(*_args, **_kwargs); end
            end
          )
        end

        it 'calls test method on test object' do
          job.perform(**arguments)

          expect(test_class).to have_received(:call).with(*args, **kwargs)
        end
      end
    end
  end

  describe 'integration testing' do
    let(:arguments) do
      {
        service_class_name: service_class_name, args: args, kwargs: kwargs
      }
    end

    around do |example|
      adapter = Rails.configuration.active_job.queue_adapter
      ActiveJob::Base.queue_adapter = :test

      example.run

      ActiveJob::Base.queue_adapter = adapter
    end

    it 'enqueues job' do
      described_class.perform_later(**arguments)

      expect(described_class).to have_been_enqueued.with(**arguments)
    end

    it 'enqueues job to correct queue' do
      described_class.perform_later(**arguments)

      expect(described_class).to have_been_enqueued.on_queue(:default)
    end
  end
end
