# frozen_string_literal: true

require 'active_support'

require 'dipa/version'
require 'dipa/engine'
require 'dipa/errors'

module Dipa
  extend ActiveSupport::Autoload

  # DEFAULT_TIMEOUT = 0
  DEFAULT_AGENT_TIMEOUT = 0
  DEFAULT_AGENT_PROCESSING_TIMEOUT = 0
  DEFAULT_COORDINATOR_TIMEOUT = 0
  DEFAULT_COORDINATOR_PROCESSING_TIMEOUT = 0

  DATETIME_PRECISION = 6

  # rubocop:disable ThreadSafety/ClassAndModuleAttributes
  mattr_accessor :agent_queue
  mattr_accessor :agent_timeout, default: DEFAULT_AGENT_TIMEOUT
  mattr_accessor :agent_processing_timeout, default: DEFAULT_AGENT_PROCESSING_TIMEOUT
  mattr_accessor :coordinator_queue
  mattr_accessor :coordinator_timeout, default: DEFAULT_COORDINATOR_TIMEOUT
  mattr_accessor :coordinator_processing_timeout, default: DEFAULT_COORDINATOR_PROCESSING_TIMEOUT
  mattr_accessor :datetime_precision, default: DATETIME_PRECISION
  # rubocop:enable ThreadSafety/ClassAndModuleAttributes

  def self.map(source, options: {})
    Dipa::Processor::Map.new(source, options: options)
  end

  def self.each(source, options: {})
    Dipa::Processor::Each.new(source, options: options)
  end

  module Processor
    extend ActiveSupport::Autoload

    module Concerns
      extend ActiveSupport::Autoload

      autoload :Coordinator
      autoload :Options
      autoload :Source
      autoload :State
      autoload :Wait
    end

    autoload :Base
    autoload :Each
    autoload :Map
  end
end
