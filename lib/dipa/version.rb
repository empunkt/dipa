# frozen_string_literal: true

# :nocov:
module Dipa
  VERSION = '0.1.0.pre.3'
end
# :nocov:
