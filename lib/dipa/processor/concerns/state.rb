# frozen_string_literal: true

module Dipa
  module Processor
    module Concerns
      module State
        extend ActiveSupport::Concern

        private

        def _processing_state?
          @_processing_state ? true : false
        end

        def _check_state
          raise Dipa::AlreadyProcessingError if _processing_state?

          @_processing_state = true
        end

        def _reset_state
          @_processing_state = false
        end
      end
    end
  end
end
