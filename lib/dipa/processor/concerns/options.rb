# frozen_string_literal: true

module Dipa
  module Processor
    module Concerns
      module Options
        extend ActiveSupport::Concern

        include ActiveSupport::Configurable

        DEFAULT_ASYNC = false
        DEFAULT_KEEP_DATA = false
        DEFAULT_WANT_RESULT = true

        OPTIONS_MAPPING = {
          # queue names
          agent_queue: { accessor: :_agent_queue, default: Dipa.agent_queue },
          coordinator_queue: { accessor: :_coordinator_queue, default: Dipa.coordinator_queue },
          # timeouts
          agent_timeout: { accessor: :_agent_timeout, default: Dipa.agent_timeout },
          agent_processing_timeout: { accessor: :_agent_processing_timeout, default: Dipa.agent_processing_timeout },
          coordinator_timeout: { accessor: :_coordinator_timeout, default: Dipa.coordinator_timeout },
          coordinator_processing_timeout: { accessor: :_coordinator_processing_timeout,
                                            default: Dipa.coordinator_processing_timeout },
          # misc
          keep_data: { accessor: :_keep_data, default: DEFAULT_KEEP_DATA },
          want_result: { accessor: :_want_result, default: DEFAULT_WANT_RESULT }
        }.freeze

        private

        included do
          OPTIONS_MAPPING.each_value do |args|
            # Since ActiveSupport < 7 does not implement `default:`` option
            # for config_accessor, defaults are set explicitly with
            # `#_write_unprovided_options`
            config_accessor(args[:accessor])
          end

          alias_method :_keep_data?, :_keep_data
          alias_method :_want_result?, :_want_result
        end

        def _validate_options(options:)
          options.assert_valid_keys(*OPTIONS_MAPPING.keys)
        end

        def _write_options(options:)
          _write_provided_options(options: options)
          _write_unprovided_options(options: options)
        end

        def _write_provided_options(options:)
          options.each { |option, value| _write_option(option, value) }
        end

        def _write_unprovided_options(options:)
          unprovided_options = OPTIONS_MAPPING.except(*options.keys)

          unprovided_options.each do |option, args|
            _write_option(option, args[:default])
          end
        end

        def _write_option(option, value)
          new_value = _value_with_override_check(option, value)
          __send__("#{OPTIONS_MAPPING[option][:accessor]}=", new_value)
        end

        def _value_with_override_check(option, value)
          if self.class::OVERRIDE_OPTIONS.key?(option)
            self.class::OVERRIDE_OPTIONS[option]
          else
            value
          end
        end
      end
    end
  end
end
