# frozen_string_literal: true

module Dipa
  module Processor
    module Concerns
      module Coordinator
        extend ActiveSupport::Concern

        include Dipa::Processor::Concerns::Options
        include Dipa::Processor::Concerns::Source

        private

        included do
          attr_reader :_coordinator
        end

        def _prepare_coordinator(processor_class:, processor_method:)
          _validate_processor_arguments(processor_class: processor_class,
                                        processor_method: processor_method)

          _create_coordinator(
            processor_class_name: processor_class,
            processor_method_name: processor_method
          )
        end

        def _validate_processor_arguments(processor_class:, processor_method:)
          return if processor_class.constantize.respond_to?(processor_method)

          raise Dipa::UnknownProcessorMethodError,
                "Method .#{processor_method} does not exist on processor " \
                "class #{processor_class}"
        rescue NameError => e
          raise Dipa::UnknownProcessorClassError, e.original_message
        end

        def _create_coordinator(processor_class_name:, processor_method_name:)
          @_coordinator = Dipa::Coordinator.create!(
            _coordinator_create_params.merge(
              {
                processor_class_name: processor_class_name,
                processor_method_name: processor_method_name
              }
            )
          )

          _coordinator.dump_to_file(data: _source.to_a, attacher: :source_dump)
        end

        def _coordinator_create_params
          {
            agent_queue: _agent_queue,
            coordinator_queue: _coordinator_queue,
            size: _source.to_a.size,
            coordinator_options_record_attributes:
              _coordinator_options_record_create_params
          }
        end

        def _coordinator_options_record_create_params
          {
            keep_data: _keep_data?,
            want_result: _want_result?,
            agent_timeout: _agent_timeout,
            agent_processing_timeout: _agent_processing_timeout,
            coordinator_timeout: _coordinator_timeout,
            coordinator_processing_timeout: _coordinator_processing_timeout
          }
        end

        def _coordinator_waiting_state?
          _coordinator.reload

          !_coordinator.finished_state?
        end

        def _result
          _coordinator.reload

          _coordinator.want_result? ? _coordinator.result : _coordinator.source
        end
      end
    end
  end
end
