# frozen_string_literal: true

module Dipa
  module Processor
    module Concerns
      module Wait
        extend ActiveSupport::Concern

        include Dipa::Processor::Concerns::Coordinator
        include Dipa::Processor::Concerns::Options

        SYNC_MODE_SLEEP_START_DURATION = 2
        SYNC_MODE_MAX_SLEEP_DURATION = 120
        SYNC_MODE_SLEEP_DURATION_INCREASE_FACTOR = 1.1

        private

        def _wait_for_it_with_timeout
          _init_timeout_and_sleep_variables

          _wait_for_it
        end

        def _init_timeout_and_sleep_variables
          @_start = Time.current
          @_sleep_duration = SYNC_MODE_SLEEP_START_DURATION
        end

        def _wait_for_it
          _sleep while _coordinator_waiting_state?

          return _result if _coordinator.finished?

          # must be an error then
          _raise_error
        end

        def _sleep
          sleep(@_sleep_duration)

          _adjust_sleep_duration
        end

        def _adjust_sleep_duration
          return if @_sleep_duration > SYNC_MODE_MAX_SLEEP_DURATION

          @_sleep_duration *= SYNC_MODE_SLEEP_DURATION_INCREASE_FACTOR
        end

        def _raise_error
          if _coordinator.failed_state?
            raise(
              "Dipa::Coordinator#{_coordinator.state.to_s.camelize}Error".constantize
            )
          end

          raise Dipa::UnknownProcessingStateError
        end
      end
    end
  end
end
