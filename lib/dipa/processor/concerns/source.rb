# frozen_string_literal: true

module Dipa
  module Processor
    module Concerns
      module Source
        extend ActiveSupport::Concern

        included do
          private

          attr_reader :_source
        end
      end
    end
  end
end
