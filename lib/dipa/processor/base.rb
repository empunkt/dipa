# frozen_string_literal: true

module Dipa
  module Processor
    class Base
      OVERRIDE_OPTIONS = {}.freeze

      include Dipa::Processor::Concerns::Coordinator
      include Dipa::Processor::Concerns::Options
      include Dipa::Processor::Concerns::Source
      include Dipa::Processor::Concerns::State
      include Dipa::Processor::Concerns::Wait

      def with(processor_class, processor_method)
        _check_state

        _prepare_coordinator(processor_class: processor_class.to_s,
                             processor_method: processor_method.to_s)

        _start_process

        _result_and_cleanup
      ensure
        _reset_state
      end

      private

      def initialize(source, options: {})
        _validate_options(options: options)
        _write_options(options: options)

        @_source = source
      end

      def _result_and_cleanup
        _wait_for_it_with_timeout
      ensure
        _after_result_action
      end

      def _after_result_action
        Dipa::CoordinatorServices::MaybeCleanupService.call(
          coordinator: _coordinator
        )
      end

      def _start_process
        Dipa::CoordinatorServices::StartProcessingService.call(
          coordinator: _coordinator
        )
      end
    end
  end
end
