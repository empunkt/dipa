# frozen_string_literal: true

module Dipa
  module Processor
    class Each < Base
      OVERRIDE_OPTIONS = {
        want_result: false
      }.freeze
    end
  end
end
