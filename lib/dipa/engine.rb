# frozen_string_literal: true

require 'socket'
require 'rails'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'

module Dipa
  class Engine < ::Rails::Engine
    isolate_namespace Dipa

    config.eager_load_namespaces << Dipa

    config.generators do |g|
      g.test_framework :rspec
      g.api_only = true
    end

    config.dipa = ActiveSupport::OrderedOptions.new

    initializer 'dipa.queue_names' do
      config.after_initialize do |app|
        Dipa.agent_queue = (
          app.config.dipa.agent_queue || app.config.active_job.default_queue_name || :default
        ).to_sym
        Dipa.coordinator_queue = (
          app.config.dipa.coordinator_queue || app.config.active_job.default_queue_name || :default
        ).to_sym
      end
    end

    initializer 'dipa.timeouts' do
      config.after_initialize do |app|
        Dipa.agent_timeout = (
          app.config.dipa.agent_timeout || Dipa::DEFAULT_AGENT_TIMEOUT
        ).to_i
        Dipa.agent_processing_timeout = (
          app.config.dipa.agent_processing_timeout || Dipa::DEFAULT_AGENT_PROCESSING_TIMEOUT
        ).to_i
        Dipa.coordinator_timeout = (
          app.config.dipa.coordinator_timeout || Dipa::DEFAULT_COORDINATOR_TIMEOUT
        ).to_i
        Dipa.coordinator_processing_timeout = (
          app.config.dipa.coordinator_processing_timeout || Dipa::DEFAULT_COORDINATOR_PROCESSING_TIMEOUT
        ).to_i
      end
    end

    initializer 'dipa.datetime_precision' do
      config.after_initialize do
        Dipa.datetime_precision = Dipa::DATETIME_PRECISION
      end
    end
  end
end
