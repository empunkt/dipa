# frozen_string_literal: true

module Dipa
  class Error < StandardError; end

  class AlreadyProcessingError < Error; end
  class UnknownProcessingStateError < Error; end

  class AgentError < Error; end
  class AgentAbortedThroughCoordinatorError < AgentError; end
  class AgentAbortedThroughCoordinatorTimedOutError < AgentAbortedThroughCoordinatorError; end
  class AgentAbortedThroughCoordinatorProcessingTimedOutError < AgentAbortedThroughCoordinatorError; end
  class AgentProcessingTimedOutError < AgentError; end
  class AgentTimedOutError < AgentError; end

  class CoordinatorError < Error; end
  class CoordinatorAbortedError < CoordinatorError; end
  class CoordinatorAbortedThroughAgentError < CoordinatorError; end
  class CoordinatorAbortedThroughAgentTimedOutError < CoordinatorAbortedThroughAgentError; end
  class CoordinatorAbortedThroughAgentProcessingTimedOutError < CoordinatorAbortedThroughAgentError; end
  class CoordinatorAbortedThroughAgentProcessingFailedError < CoordinatorAbortedThroughAgentError; end
  class CoordinatorAbortedThroughAgentProcessingFailureError < CoordinatorAbortedThroughAgentError; end
  class CoordinatorProcessingFailedError < CoordinatorError; end
  class CoordinatorProcessingTimedOutError < CoordinatorError; end
  class CoordinatorTimedOutError < CoordinatorError; end

  class UnknownProcessorClassError < Error; end
  class UnknownProcessorMethodError < Error; end
end
