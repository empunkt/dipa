# Changelog

## Scheduled for next version

*   Bump version and update changelog
*   Fix finished state evaluation
*   Update version
*   Reduce workflows
*   Reactivate all CI steps
*   Fix timeout error handling
*   Add testapp rspec
*   Fix setting queue names
*   Fix determination of failed state
*   Add .testapp_test.yml
*   Load annotate in Rakefile instead of having a dedicated *.rake file. Fixes LoadError in case the rails app does not use annotate gem
*   Make database config more flexible
*   Update README and minor format adjustments
*   Fix time precision issue
*   Fix rails 6 and ruby 3.0 version compatibility issues
*   update woodpecker files
*   Fix tests and add missing tests plus minor refactorings
*   Finally exept rubocops "new" line length default
*   Enforce datetime precision 6
*   Update .gitignore
*   Reformat changelog
*   Update changelog
*   Update rbs libraries
*   Add asynchronous timeout handling
*   Move constants to concern
*   Format README
*   Upgrade to ruby 3.2 for rubocop
*   Fix rubocop offenses
*   Update rbs libraries
*   Add latest ruby/rails and remove EOL ruby/rails
*   Add flake.nix
*   Fix rubocop offenses
*   Update changelog
*   Add changelog generator
*   Add asynchronous cleanup
*   Add security check before processing
*   Fix options handling for rails < 7
*   Split up Dipa::Processor:Base class
*   Add specific timeouts
*   Update rbs collection
*   Reduce build resource consumption

## v0.1.0.pre.1 - 2022-03-05

*   Remove unused configuration and comments
*   Update rbs collection
*   Fix rubocop
*   Drop automated push for now
*   Document configuration and usage in README.md
*   Add annotate gem

    annotate gem didn't work for rails 7 but has been fixed now
*   add build and push pipeline
*   refactoring
*   add custom processor method
*   update appraisals
*   update rbs collection
*   make pipeline more resource saving
*   some refactoring
*   remove unused configuration options
*   validator specs
*   coordinator services specs
*   agent services specs
*   model specs
*   remove obsolete configuration options
*   remove unused services and refactor MapService
*   Didn't finish first iteration since ADHD kicked in and forced me to switch to ActiveStorage immediately. Makes a lot of things obsolete and is more convenient.
*   specs for Dipa::AgentServices::ProcessingService
*   use build matrix instead of appraisal
*   readd ruby versions
*   ruby 3.1 and rails 7 only for now
*   spacs for Dipa::AgentServices::PostProcessingService
*   update appraisals
*   - some rubocop fixes
    - added TODO.md
*   remove duplicated coordinator update from Dipa::CoordinatorServices::StartProcessingService
*   specs for Dipa::AgentServices::CoordinatorStateService
*   refactor a lot
*   specs for callback job
*   add database port to DATABASE_URL
*   add RAILS_ENV to rspec builds
*   POC
*   Initial adjustments
*   Initial commit

