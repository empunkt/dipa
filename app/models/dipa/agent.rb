# frozen_string_literal: true

# == Schema Information
#
# Table name: dipa_agents
#
#  id                  :bigint           not null, primary key
#  finished_at         :datetime
#  index               :integer          not null
#  started_at          :datetime
#  state               :string(255)      not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  dipa_coordinator_id :bigint           not null
#
# Indexes
#
#  index_dipa_agents_on_dipa_coordinator_id  (dipa_coordinator_id)
#
# Foreign Keys
#
#  fk_rails_...  (dipa_coordinator_id => dipa_coordinators.id)
#

module Dipa
  class Agent < ApplicationRecord
    include Dipa::Models::Dumpable
    include Dipa::Models::Loadable
    include Dipa::Models::AgentStateAttributes
    include Dipa::Models::StateAttributeHandling

    # validation and default for `state` attribute is included by
    # Dipa::Models::StateAttributeHandling

    validates :index, numericality: { only_integer: true }

    validates :started_at, 'dipa/date' => true, allow_nil: true
    validates :finished_at, 'dipa/date' => true, allow_nil: true

    belongs_to :coordinator, inverse_of: :agents,
                             foreign_key: :dipa_coordinator_id

    has_one_attached :source_dump
    has_one_attached :result_dump

    def result
      return unless finished?

      load_from_file(attacher: :result_dump)
    end

    def source
      load_from_file(attacher: :source_dump)
    end

    def timeout_reached?
      return false if coordinator.agent_timeout.zero?

      (created_at + coordinator.agent_timeout) < Time.current.floor(Dipa::DATETIME_PRECISION)
    end

    def processing_timeout_reached?
      return false if coordinator.agent_processing_timeout.zero?

      (started_at + coordinator.agent_processing_timeout) < Time.current.floor(Dipa::DATETIME_PRECISION)
    end
  end
end
