# frozen_string_literal: true

# == Schema Information
#
# Table name: dipa_coordinators
#
#  id                    :bigint           not null, primary key
#  agent_queue           :string(255)      not null
#  coordinator_queue     :string(255)      not null
#  finished_at           :datetime
#  processor_class_name  :string(255)      not null
#  processor_method_name :string(255)      not null
#  size                  :integer          not null
#  started_at            :datetime
#  state                 :string(255)      not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

module Dipa
  class Coordinator < ApplicationRecord
    include Dipa::Models::Dumpable
    include Dipa::Models::Loadable
    include Dipa::Models::CoordinatorStateAttributes
    include Dipa::Models::StateAttributeHandling

    # validation and default for `state` attribute is included by
    # Dipa::Models::StateAttributeHandling

    validates :agent_queue, presence: true
    validates :coordinator_queue, presence: true

    validates :size, numericality: { only_integer: true }
    validates :started_at, 'dipa/date' => true, allow_nil: true
    validates :finished_at, 'dipa/date' => true, allow_nil: true
    validates :processor_class_name, presence: true
    validates :processor_method_name, presence: true

    has_many :agents, dependent: :destroy, inverse_of: :coordinator,
                      foreign_key: :dipa_coordinator_id

    has_one :coordinator_options_record, dependent: :destroy,
                                         inverse_of: :coordinator,
                                         foreign_key: :dipa_coordinator_id
    accepts_nested_attributes_for :coordinator_options_record

    has_one_attached :source_dump

    delegate :keep_data?, :want_result?,
             :agent_timeout, :agent_processing_timeout,
             :coordinator_timeout, :coordinator_processing_timeout,
             to: :coordinator_options_record

    def result
      return unless finished?

      _result_from_agents
    end

    def source
      load_from_file(attacher: :source_dump)
    end

    def all_agents_created_and_finished?
      _all_agents_created? && _all_agents_finished?
    end

    def timeout_reached?
      return false if coordinator_timeout.zero?

      (created_at + coordinator_timeout) < Time.current.floor(Dipa::DATETIME_PRECISION)
    end

    def processing_timeout_reached?
      return false if coordinator_processing_timeout.zero?

      (started_at + coordinator_processing_timeout) < Time.current.floor(Dipa::DATETIME_PRECISION)
    end

    private

    def _result_from_agents
      agents.with_attached_result_dump.order(:index).map(&:result)
    end

    def _all_agents_finished?
      agents.all?(&:finished?)
    end

    def _all_agents_created?
      agents.length == size
    end
  end
end
