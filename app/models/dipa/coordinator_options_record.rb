# frozen_string_literal: true

# == Schema Information
#
# Table name: dipa_coordinator_options_records
#
#  id                             :bigint           not null, primary key
#  agent_processing_timeout       :integer          not null
#  agent_timeout                  :integer          not null
#  coordinator_processing_timeout :integer          not null
#  coordinator_timeout            :integer          not null
#  keep_data                      :boolean          default(FALSE), not null
#  want_result                    :boolean          default(TRUE), not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  dipa_coordinator_id            :bigint           not null
#
# Indexes
#
#  index_dipa_coordinator_options_records_on_dipa_coordinator_id  (dipa_coordinator_id)
#
# Foreign Keys
#
#  fk_rails_...  (dipa_coordinator_id => dipa_coordinators.id)
#

module Dipa
  class CoordinatorOptionsRecord < ApplicationRecord
    attribute :keep_data, :boolean, default: false
    attribute :want_result, :boolean, default: true

    attribute :agent_timeout, :integer
    attribute :agent_processing_timeout, :integer
    attribute :coordinator_timeout, :integer
    attribute :coordinator_processing_timeout, :integer

    validates :keep_data, inclusion: [true, false]
    validates :want_result, inclusion: [true, false]

    validates :agent_timeout,
              numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :agent_processing_timeout,
              numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :coordinator_timeout,
              numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :coordinator_processing_timeout,
              numericality: { only_integer: true, greater_than_or_equal_to: 0 }

    belongs_to :coordinator, inverse_of: :coordinator_options_record,
                             foreign_key: :dipa_coordinator_id
  end
end
