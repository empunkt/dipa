# frozen_string_literal: true

module Dipa
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end
