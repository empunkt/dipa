# frozen_string_literal: true

module Dipa
  module Models
    module SettingConstants
      extend ActiveSupport::Concern

      class_methods do
        def set_constant(konst:, value:)
          return if const_defined?(konst)

          const_set(konst, value)
        end

        def set_from_merged_constants(konst:, first_from:, second_from:)
          merged = const_get(first_from).merge(const_get(second_from))

          set_constant(konst: konst, value: merged)
        end
      end
    end
  end
end
