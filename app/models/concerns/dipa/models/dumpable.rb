# frozen_string_literal: true

module Dipa
  module Models
    module Dumpable
      extend ActiveSupport::Concern

      def dump_to_file(data:, attacher:)
        io = StringIO.new(Marshal.dump(data), 'rb')
        filename = "#{attacher}.dat"

        __send__(attacher).attach(io: io, filename: filename)
      end
    end
  end
end
