# frozen_string_literal: true

module Dipa
  module Models
    module StateAttributeHandling
      extend ActiveSupport::Concern

      include Dipa::Models::SettingConstants

      SHARED_FINISHED_STATES = {
        aborted: 'aborted',
        finished: 'finished',
        processing_failed: 'processing_failed',
        processing_timed_out: 'processing_timed_out',
        timed_out: 'timed_out'
      }.freeze
      SHARED_CONTINUING_STATES = {
        initialized: 'initialized',
        processing: 'processing'
      }.freeze

      included do |base|
        set_constant(konst: :SHARED_FINISHED_STATES, value: SHARED_FINISHED_STATES)
        set_constant(konst: :SHARED_CONTINUING_STATES, value: SHARED_CONTINUING_STATES)

        set_from_merged_constants(konst: :COMBINED_FINISHED_STATES,
                                  first_from: :SHARED_FINISHED_STATES, second_from: :EXTRA_FINISHED_STATES)
        set_from_merged_constants(konst: :COMBINED_CONTINUING_STATES,
                                  first_from: :SHARED_CONTINUING_STATES, second_from: :EXTRA_CONTINUING_STATES)
        set_from_merged_constants(konst: :COMBINED_STATES,
                                  first_from: :COMBINED_FINISHED_STATES, second_from: :COMBINED_CONTINUING_STATES)

        # :nocov: Only one branch can be covered here
        if Rails.version >= '7'
          enum :state, base::COMBINED_STATES, default: base::COMBINED_STATES[:initialized]
          validates :state, comparison: { equal_to: base::COMBINED_STATES[:initialized] }, on: :create
        else
          enum state: base::COMBINED_STATES, _default: base::COMBINED_STATES[:initialized]
          validates :state, inclusion: { in: [base::COMBINED_STATES[:initialized]] }, on: :create
        end
        # :nocov:

        validates :state, presence: true, inclusion: { in: base::COMBINED_STATES.values }

        before_validation :_set_started_at, if: :_processing_started?
        before_validation :_set_finished_at, if: :_processing_finished?
      end

      def finished_state?
        self.class::COMBINED_FINISHED_STATES.value?(state)
      end

      def failed_state?
        self.class::COMBINED_FINISHED_STATES.except(:finished).value?(state)
      end

      private

      def _set_started_at
        self.started_at = Time.current
      end

      def _set_finished_at
        self.finished_at = Time.current
      end

      def _processing_started?
        state_changed?(from: self.class::COMBINED_STATES[:initialized], to: self.class::COMBINED_STATES[:processing])
      end

      def _processing_finished?
        state_changed?(from: self.class::COMBINED_STATES[:processing]) &&
          self.class::COMBINED_FINISHED_STATES.value?(state_change[1])
      end
    end
  end
end
