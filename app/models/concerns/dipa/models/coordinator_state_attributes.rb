# frozen_string_literal: true

module Dipa
  module Models
    module CoordinatorStateAttributes
      extend ActiveSupport::Concern

      include Dipa::Models::SettingConstants

      COORDINATOR_EXTRA_FINISHED_STATES = {
        aborted_through_agent: 'aborted_through_agent',
        aborted_through_agent_timed_out: 'aborted_through_agent_timed_out',
        aborted_through_agent_processing_timed_out: 'aborted_through_agent_processing_timed_out',
        aborted_through_agent_processing_failed: 'aborted_through_agent_processing_failed'
      }.freeze
      COORDINATOR_EXTRA_CONTINUING_STATES = {}.freeze

      included do
        set_constant(konst: :EXTRA_FINISHED_STATES, value: COORDINATOR_EXTRA_FINISHED_STATES)
        set_constant(konst: :EXTRA_CONTINUING_STATES, value: COORDINATOR_EXTRA_CONTINUING_STATES)
      end
    end
  end
end
