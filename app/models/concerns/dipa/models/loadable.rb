# frozen_string_literal: true

module Dipa
  module Models
    module Loadable
      extend ActiveSupport::Concern

      def load_from_file(attacher:)
        return unless __send__(attacher).attached?

        Marshal.load( # rubocop:disable Security/MarshalLoad
          __send__(attacher).download
        )
      end
    end
  end
end
