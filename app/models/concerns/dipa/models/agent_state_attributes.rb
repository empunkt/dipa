# frozen_string_literal: true

module Dipa
  module Models
    module AgentStateAttributes
      extend ActiveSupport::Concern

      include Dipa::Models::SettingConstants

      AGENT_EXTRA_FINISHED_STATES = {
        processing_failed: 'processing_failed',
        aborted_through_coordinator: 'aborted_through_coordinator',
        aborted_through_coordinator_timed_out: 'aborted_through_coordinator_timed_out',
        aborted_through_coordinator_processing_timed_out: 'aborted_through_coordinator_processing_timed_out'
      }.freeze
      AGENT_EXTRA_CONTINUING_STATES = {}.freeze

      included do
        set_constant(konst: :EXTRA_FINISHED_STATES, value: AGENT_EXTRA_FINISHED_STATES)
        set_constant(konst: :EXTRA_CONTINUING_STATES, value: AGENT_EXTRA_CONTINUING_STATES)
      end
    end
  end
end
