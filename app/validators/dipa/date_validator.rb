# frozen_string_literal: true

module Dipa
  class DateValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      return if self.class.valid?(value: value)

      record.errors.add(
        attribute,
        :invalid,
        message: (options[:message] || 'is not valid Date')
      )
    end

    def self.valid?(value:)
      return false if value.blank?

      begin
        Date.parse(value.to_s)
        true
      rescue ArgumentError
        false
      end
    end
  end
end
