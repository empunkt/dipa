# frozen_string_literal: true

module Dipa
  class ServiceJob < ApplicationJob
    def perform(service_class_name:, args: [], kwargs: {})
      service_class_name.constantize.call(*args, **kwargs)
    end
  end
end
