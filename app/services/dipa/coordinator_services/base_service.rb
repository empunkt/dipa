# frozen_string_literal: true

module Dipa
  module CoordinatorServices
    class BaseService < ApplicationService
      def call(coordinator:)
        @_coordinator = coordinator
      end

      private

      attr_reader :_coordinator

      def initialize
        super

        @_coordinator = nil
      end
    end
  end
end
