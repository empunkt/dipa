# frozen_string_literal: true

module Dipa
  module CoordinatorServices
    class CreateAgentsService < Dipa::CoordinatorServices::BaseService
      def call(coordinator:)
        super

        _coordinator.source.each_with_index do |item, i|
          _create_agent(item: item, index: i)
        end
      end

      private

      def _create_agent(item:, index:)
        agent = _coordinator.agents.create!(index: index)

        agent.dump_to_file(data: item, attacher: :source_dump)

        Dipa::AgentServices::StartProcessingService.call(agent: agent)
      end
    end
  end
end
