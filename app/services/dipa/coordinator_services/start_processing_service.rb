# frozen_string_literal: true

module Dipa
  module CoordinatorServices
    class StartProcessingService < Dipa::CoordinatorServices::BaseService
      def call(coordinator:)
        super

        _coordinator.processing!

        ServiceJob.set(queue: _coordinator.coordinator_queue)
                  .perform_later(
                    service_class_name:
                      'Dipa::CoordinatorServices::CreateAgentsService',
                    kwargs: { coordinator: _coordinator }
                  )
      end
    end
  end
end
