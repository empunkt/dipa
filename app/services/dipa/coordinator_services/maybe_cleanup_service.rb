# frozen_string_literal: true

module Dipa
  module CoordinatorServices
    class MaybeCleanupService < Dipa::CoordinatorServices::BaseService
      def call(coordinator:)
        super

        return if _coordinator.keep_data?

        Dipa::ServiceJob.set(queue: _coordinator.coordinator_queue)
                        .perform_later(
                          service_class_name:
                            'Dipa::CoordinatorServices::DestroyService',
                          kwargs: { coordinator: _coordinator }
                        )
      end
    end
  end
end
