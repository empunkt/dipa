# frozen_string_literal: true

module Dipa
  module CoordinatorServices
    class DestroyService < Dipa::CoordinatorServices::BaseService
      def call(coordinator:)
        super

        _coordinator&.destroy!
      end
    end
  end
end
