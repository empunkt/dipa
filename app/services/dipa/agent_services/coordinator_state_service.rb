# frozen_string_literal: true

module Dipa
  module AgentServices
    class CoordinatorStateService < Dipa::AgentServices::BaseService
      AGENT_STATES_TO_CHECK_FOR = %i[
        timed_out
        processing_timed_out
        processing_failed
      ].freeze

      COORDINATOR_STATES_TO_CHECK_FOR = %i[
        aborted_through_coordinator_timed_out
        aborted_through_coordinator_processing_timed_out
      ].freeze

      def call(agent:)
        super

        return if _agent.reload.coordinator.finished_state?

        _handle_agent_states
      end

      private

      def _handle_agent_states
        if _agent.reload.failed_state?
          _handle_agent_failed_states
        else
          _handle_agent_success_states
        end
      end

      def _determine_coordinator_failed_state
        _determine_aborted_through_coordinator_state || _determine_aborted_through_agent_state || :aborted_through_agent
      end

      def _determine_aborted_through_agent_state
        state = _agent.reload.state

        return unless AGENT_STATES_TO_CHECK_FOR.include?(state.to_sym)

        "aborted_through_agent_#{state}".to_sym
      end

      def _determine_aborted_through_coordinator_state
        state = _agent.reload.state

        return unless COORDINATOR_STATES_TO_CHECK_FOR.include?(state.to_sym)

        state.delete_prefix('aborted_through_coordinator_').to_sym
      end

      def _handle_agent_failed_states
        state = _determine_coordinator_failed_state

        _agent.coordinator.__send__("#{state}!".to_sym)
      end

      def _handle_agent_success_states
        return unless _agent.reload.coordinator.all_agents_created_and_finished?

        _agent.coordinator.finished!
      end
    end
  end
end
