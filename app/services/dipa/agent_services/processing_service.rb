# frozen_string_literal: true

module Dipa
  module AgentServices
    class ProcessingService < Dipa::AgentServices::BaseService
      include Dipa::Services::ProcessFailureHandling

      TIMER_TASK_SLEEP_START_DURATION = 2
      TIMER_TASK_MAX_SLEEP_DURATION = 120
      TIMER_TASK_SLEEP_DURATION_INCREASE_FACTOR = 1.1
      TIMER_TASK_ARGS = {
        execution_interval: TIMER_TASK_SLEEP_START_DURATION
      }.freeze

      def call(agent:)
        super

        _process!
      ensure
        _post_processing
      end

      private

      attr_reader :_processor_thread

      def initialize
        super

        @_processor_thread = Thread.current
      end

      def _process!
        _start_monitoring_task

        processor_result = _processor_result

        _agent.dump_to_file(data: processor_result, attacher: :result_dump)
        _agent.finished!
      rescue StandardError => e
        _agent.processing_failed! unless e.is_a?(Dipa::AgentError)
      end

      def _processor_result
        _agent.coordinator.processor_class_name.constantize
              .__send__(
                _agent.coordinator.processor_method_name,
                _agent.source
              )
      end

      def _monitoring_task
        @_monitoring_task ||= Concurrent::TimerTask.new(TIMER_TASK_ARGS, &_monitoring_task_iteration_block)
      end

      def _monitoring_task_iteration_block
        proc { |task| _monitoring_task_iteration(task: task) }
      end

      def _monitoring_task_iteration(task:)
        execution_context = Rails.application.executor.run!

        _handle_execution_interval(task: task)

        return unless _stop_processing?(agent: _agent)

        _stop_processing!(agent: _agent, thread: _processor_thread)

        task.shutdown
      ensure
        execution_context&.complete!
      end

      def _handle_execution_interval(task:)
        return if _max_execution_interval_reached?(task: task)

        task.execution_interval *= TIMER_TASK_SLEEP_DURATION_INCREASE_FACTOR
      end

      def _max_execution_interval_reached?(task:)
        task.execution_interval > TIMER_TASK_MAX_SLEEP_DURATION
      end

      def _start_monitoring_task
        _monitoring_task.execute
      end

      def _post_processing
        _monitoring_task.shutdown if _monitoring_task.running?
        Dipa::AgentServices::PostProcessingService.call(agent: _agent)
      end
    end
  end
end
