# frozen_string_literal: true

module Dipa
  module AgentServices
    class PostProcessingService < Dipa::AgentServices::BaseService
      def call(agent:)
        super

        Dipa::ServiceJob.set(queue: _agent.coordinator.agent_queue)
                        .perform_later(
                          service_class_name:
                            'Dipa::AgentServices::CoordinatorStateService',
                          kwargs: { agent: _agent }
                        )
      end
    end
  end
end
