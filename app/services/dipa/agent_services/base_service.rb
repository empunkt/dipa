# frozen_string_literal: true

module Dipa
  module AgentServices
    class BaseService < ApplicationService
      def call(agent:)
        @_agent = agent
      end

      private

      attr_reader :_agent

      def initialize
        super

        @_agent = nil
      end
    end
  end
end
