# frozen_string_literal: true

module Dipa
  module AgentServices
    class StartProcessingService < Dipa::AgentServices::BaseService
      def call(agent:)
        super

        _agent.processing!

        Dipa::ServiceJob.set(queue: _agent.coordinator.agent_queue)
                        .perform_later(
                          service_class_name:
                            'Dipa::AgentServices::ProcessingService',
                          kwargs: { agent: _agent }
                        )
      end
    end
  end
end
