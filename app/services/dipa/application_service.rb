# frozen_string_literal: true

module Dipa
  class ApplicationService
    def self.call(...)
      new.call(...)
    end
  end
end
