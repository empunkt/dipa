# frozen_string_literal: true

module Dipa
  module Services
    module ProcessFailureHandling
      extend ActiveSupport::Concern

      # Order of the checks is important here
      STATES_TO_CHECK_FOR = %i[
        aborted_through_coordinator
        aborted_through_coordinator_timed_out
        aborted_through_coordinator_processing_timed_out
        timed_out
        processing_timed_out
      ].freeze

      private

      def _stop_processing?(agent:)
        STATES_TO_CHECK_FOR.any? do |state|
          __send__("_#{state}_state_desired?".to_sym, agent: agent)
        end
      end

      def _stop_processing!(agent:, thread:)
        agent_state = _determine_agent_state(agent: agent)

        error_class_name = _determine_error_class_name(state: agent_state)

        _end_it_all!(agent: agent, thread: thread, state: agent_state, error_class_name: error_class_name)
      end

      def _determine_error_class_name(state:)
        "Dipa::Agent#{state.to_s.camelize}Error"
      end

      def _determine_agent_state(agent:)
        _determine_agent_current_state(agent: agent) || _determine_agent_desired_state(agent: agent)
      end

      def _determine_agent_current_state(agent:)
        return unless agent.reload.failed_state?

        agent.state.to_sym
      end

      def _determine_agent_desired_state(agent:)
        STATES_TO_CHECK_FOR.each do |state|
          return state if __send__("_#{state}_state_desired?", agent: agent)
        end
      end

      def _aborted_through_coordinator_state_desired?(agent:)
        agent.reload.coordinator.failed_state?
      end

      def _aborted_through_coordinator_timed_out_state_desired?(agent:)
        agent.reload.coordinator.timeout_reached?
      end

      def _aborted_through_coordinator_processing_timed_out_state_desired?(agent:)
        agent.reload.coordinator.processing_timeout_reached?
      end

      def _timed_out_state_desired?(agent:)
        agent.reload.timeout_reached?
      end

      def _processing_timed_out_state_desired?(agent:)
        agent.reload.processing_timeout_reached?
      end

      def _end_it_all!(agent:, thread:, state:, error_class_name:)
        agent.__send__("#{state}!") unless agent.reload.__send__("#{state}?")

        thread.raise error_class_name.constantize
      end
    end
  end
end
