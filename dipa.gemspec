# frozen_string_literal: true

require_relative 'lib/dipa/version'

Gem::Specification.new do |spec|
  spec.name = 'dipa'
  spec.version = Dipa::VERSION
  spec.authors = ['Merten Falk']
  spec.email = ['empunkt@mailbox.org']

  spec.summary = 'Rails engine that provides an API to execute code in ' \
                 'parallel and distributed using the rails ecosystem.'
  spec.description = <<~DESC
    This gem provides an API for parallel processing like the parallel gem but
    distributed and scalable over different machines. All this with minimum
    configuration and minimum dependencies to specific technologies and using
    the rails ecosystem.

    Dipa provides a rails engine which depends on ActiveJob and ActiveStorage.
    You can use whatever backend you like for any of this components and
    configure them for your specific usecase.

    The purpose of this gem is to distribute load heavy and long running
    processing of large datasets over multiple processes or machines using
    ActiveJob.
  DESC
  spec.homepage = 'https://codeberg.org/empunkt/dipa'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'

  spec.metadata = {
    'allowed_push_host' => 'https://rubygems.org',
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' =>
      'https://codeberg.org/empunkt/dipa/src/branch/main/CHANGELOG.md',
    'rubygems_mfa_required' => 'true'
  }

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,db,lib}/**/*', 'LICENSE.txt', 'Rakefile', 'README.md']
  end
  spec.bindir = 'exe'
  spec.executables = []
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  required_rails_version = ['> 6.1.0', '< 8.0.0']
  spec.add_runtime_dependency 'activejob', *required_rails_version
  spec.add_runtime_dependency 'activerecord', *required_rails_version
  spec.add_runtime_dependency 'activestorage', *required_rails_version
  spec.add_runtime_dependency 'activesupport', *required_rails_version
  spec.add_runtime_dependency 'rake', '~> 13.0'
  spec.add_runtime_dependency 'validates_timeliness'
end
