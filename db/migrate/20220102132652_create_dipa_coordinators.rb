# frozen_string_literal: true

class CreateDipaCoordinators < ActiveRecord::Migration[6.0]
  def change # rubocop:disable Metrics/MethodLength
    create_table :dipa_coordinators do |t|
      t.datetime :finished_at, precision: Dipa.datetime_precision
      t.datetime :started_at, precision: Dipa.datetime_precision
      t.integer :size, null: false
      t.string :agent_queue, null: false
      t.string :coordinator_queue, null: false
      t.string :processor_class_name, null: false
      t.string :processor_method_name, null: false
      t.string :state, null: false

      t.timestamps precision: Dipa.datetime_precision
    end
  end
end
