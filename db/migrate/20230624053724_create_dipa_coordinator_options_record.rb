# frozen_string_literal: true

class CreateDipaCoordinatorOptionsRecord < ActiveRecord::Migration[6.0]
  def change # rubocop:disable Metrics/MethodLength
    create_table :dipa_coordinator_options_records do |t|
      t.boolean :keep_data, default: false, null: false
      t.boolean :want_result, default: true, null: false
      t.integer :agent_timeout, null: false
      t.integer :agent_processing_timeout, null: false
      t.integer :coordinator_timeout, null: false
      t.integer :coordinator_processing_timeout, null: false

      t.timestamps precision: Dipa.datetime_precision
    end

    add_belongs_to(
      :dipa_coordinator_options_records, :dipa_coordinator,
      foreign_key: true, index: true, null: false
    )
  end
end
