# frozen_string_literal: true

class CreateDipaAgents < ActiveRecord::Migration[6.0]
  def change # rubocop:disable Metrics/MethodLength
    create_table :dipa_agents do |t|
      t.datetime :finished_at, precision: Dipa.datetime_precision
      t.datetime :started_at, precision: Dipa.datetime_precision
      t.integer :index, null: false
      t.string :state, null: false

      t.timestamps precision: Dipa.datetime_precision
    end

    add_belongs_to(
      :dipa_agents, :dipa_coordinator,
      foreign_key: true, index: true, null: false
    )
  end
end
